import torch


class FeynmanIntegralsModel(torch.nn.Module):
    def __init__(self, num_in, hidden_layers, num_eps, num_func, device):
        super().__init__()

        self.dtype_real = torch.float32
        self.dtype_complex = torch.complex64

        self.num_in = num_in
        self.num_eps = num_eps
        self.num_func = num_func
        self.output_size = num_func * num_eps * 2
        layers = [num_in] + hidden_layers

        self.stacked = torch.nn.Sequential(
            *[
                f(x)
                for x in zip(layers[:-1], layers[1:])
                for f in (
                    lambda x: torch.nn.Linear(*x, dtype=self.dtype_real, device=device),
                    lambda _: torch.nn.Tanh(),
                )
            ],
            torch.nn.Linear(
                layers[-1], self.output_size, dtype=self.dtype_real, device=device
            ),
        )

        for i in range(0, len(self.stacked), 2):
            torch.nn.init.xavier_uniform_(
                self.stacked[i].weight, gain=torch.nn.init.calculate_gain("tanh")
            )

    def forward(self, x):
        return self.stacked(x)

    def as_real(self, x):
        return self.forward(x).view(len(x), self.num_func, self.num_eps, 2)

    def as_complex(self, x):
        return torch.view_as_complex(self.as_real(x))
