#!/usr/bin/env python3

import torch, numpy

from generate_data import sol
import graphs, config
from model import FeynmanIntegralsModel


def line(x):
    m = (x[-1, 1] - x[0, 1]) / (x[-1, 0] - x[0, 0])
    c = x[0, 1] - m * x[0, 0]
    return m, c


if __name__ == "__main__":
    print("Initialising")

    # config

    cfg = config.open_config("box.json")

    device = cfg["device"]
    model_file = cfg["model_filename"]
    hidden_layers = cfg["hyperparameters"]["architecture"]["hidden_layers"]
    num_in = cfg["system"]["input"]["variables"]
    num_eps = cfg["system"]["output"]["epsilon_orders"]
    num_func = cfg["system"]["output"]["functions"]
    domain_s_l, domain_s_h = cfg["system"]["input"]["boundaries"][0]
    domain_t_l, domain_t_h = cfg["system"]["input"]["boundaries"][1]
    vx = cfg["validation"]["input"]
    vy = cfg["validation"]["output"]

    # model

    model = FeynmanIntegralsModel(num_in, hidden_layers, num_eps, num_func, device)
    model.load_state_dict(torch.load(model_file))
    model.eval()

    # validation statistics

    validation_statistics = numpy.load("validation-statistics.npy")

    graphs.plot_validation_statistics(
        *validation_statistics,
        name="box",
    )

    # # slice plot 1

    # n = 1024
    # s1_line = numpy.linspace(domain_s_l, domain_s_h, n)

    # t1 = 0.5 * (domain_t_l + domain_t_h)
    # x1_slice = numpy.stack((s1_line, numpy.full(n, t1)), axis=1)

    # ff1 = (
    #     numpy.array([sol(v) for v in x1_slice], dtype=numpy.double)
    #     .view(dtype=numpy.cdouble)
    #     .squeeze()
    # )

    # x1 = torch.tensor(
    #     x1_slice,
    #     device=device,
    #     dtype=model.dtype_real,
    # )

    # with torch.no_grad():
    #     nn1 = model.as_complex(x1)

    # nn1 = nn1.cpu().numpy()

    # graphs.plot_validation_inference(
    #     s1_line, ff1, nn1, "$s$", f"$f_i^{{(j)}}(s, t={t1:.1f})$", "fixed-t"
    # )

    # # slice plot 2

    # t2_line = numpy.linspace(domain_t_l, domain_t_h, n)
    # x2_slice = numpy.stack((s1_line, t2_line), axis=1)

    # m2, c2 = line(x2_slice)

    # ff2 = (
    #     numpy.array([sol(v) for v in x2_slice], dtype=numpy.double)
    #     .view(dtype=numpy.cdouble)
    #     .squeeze()
    # )

    # x2 = torch.tensor(
    #     x2_slice,
    #     device=device,
    #     dtype=model.dtype_real,
    # )

    # with torch.no_grad():
    #     nn2 = model.as_complex(x2)

    # nn2 = nn2.cpu().numpy()

    # graphs.plot_validation_inference(
    #     s1_line, ff2, nn2, "$s$", f"$f_i^{{(j)}}(s, t={c2:.1f}+{m2:.1f}s)$", "diag-t"
    # )

    # # slice plot 3

    # s3 = 0.5 * (domain_s_l + domain_s_h)
    # x3_slice = numpy.stack((numpy.full(n, s3), t2_line), axis=1)

    # ff3 = (
    #     numpy.array([sol(v) for v in x3_slice], dtype=numpy.double)
    #     .view(dtype=numpy.cdouble)
    #     .squeeze()
    # )

    # x3 = torch.tensor(
    #     x3_slice,
    #     device=device,
    #     dtype=model.dtype_real,
    # )

    # with torch.no_grad():
    #     nn3 = model.as_complex(x3)

    # nn3 = nn3.cpu().numpy()

    # graphs.plot_validation_inference(
    #     t2_line, ff3, nn3, "$t$", f"$f_i^{{(j)}}(s={s3:.1f}, t)$", "fixed-s"
    # )

    # # slice plot 4

    # t4_line = numpy.linspace(domain_t_h, domain_t_l, n)
    # x4_slice = numpy.stack((s1_line, t4_line), axis=1)

    # m4, c4 = line(x4_slice)

    # ff4 = (
    #     numpy.array([sol(v) for v in x4_slice], dtype=numpy.double)
    #     .view(dtype=numpy.cdouble)
    #     .squeeze()
    # )

    # x4 = torch.tensor(
    #     x4_slice,
    #     device=device,
    #     dtype=model.dtype_real,
    # )

    # with torch.no_grad():
    #     nn4 = model.as_complex(x4)

    # nn4 = nn4.cpu().numpy()

    # graphs.plot_validation_inference(
    #     s1_line, ff4, nn4, "$s$", f"$f_i^{{(j)}}(s, t={c4:.1f}{m4:.1f}s)$", "antidiag-t"
    # )

    # histogram of deviation from validation dataset

    x0 = torch.tensor(
        vx,
        device=device,
        dtype=model.dtype_real,
    )

    with torch.no_grad():
        nn0 = model.as_complex(x0)

    nn0 = nn0.cpu().numpy()

    vy = numpy.array(vy, dtype=numpy.double).view(dtype=numpy.cdouble).squeeze()

    graphs.plot_validation_deviation(vy, nn0, (0.1,))
