#!/usr/bin/env python3

import math, json
import numpy, mpmath
import config


def step(x):
    return float(x >= 0)


def sol(v):
    # i^th row for function j: [f_0, f_1, f_2]
    # j^th column for coefficient of eps^i: [eps^0, eps^1, eps^2, eps^3]
    s = v[0]
    t = v[1]
    delta = 1e-10

    f = (
        numpy.array(
            [
                [
                    0,
                    1,
                    2 - math.log(-t),
                    4 - math.pi**2 / 12 - 2 * math.log(-t) + math.log(-t) ** 2 / 2,
                ],
                [
                    0,
                    1,
                    2 - (math.log(s) - math.pi * 1j),
                    (
                        4
                        - math.pi**2 / 12
                        - 2 * (math.log(s) - math.pi * 1j)
                        + (math.log(s) - math.pi * 1j) ** 2 / 2
                    ),
                ],
                [
                    4 / (s * t),
                    -2 * (math.log(s) - math.pi * 1j + math.log(-t)) / (s * t),
                    (
                        -4 * math.pi**2
                        + 6 * (math.log(s) - math.pi * 1j) * math.log(-t)
                    )
                    / (3 * s * t),
                    (
                        7 * math.pi**2 * (-1j * math.pi + math.log(s))
                        + 6 * (-1j * math.pi + math.log(s)) ** 3
                        + 7 * math.pi**2 * math.log(-t)
                        + 12 * (math.pi + 1j * math.log(s)) ** 2 * math.log(-t)
                        + 2 * math.log(-t) ** 3
                        - 12
                        * (math.pi + 1j * math.log(s))
                        * math.log(-t)
                        * (math.pi * step(s + t) + 1j * math.log(abs(s + t)))
                        - 6
                        * math.pi**2
                        * (-1j * math.pi * step(s + t) + math.log(abs(s + t)))
                        + 6
                        * (math.pi + 1j * math.log(s)) ** 2
                        * (-1j * math.pi * step(s + t) + math.log(abs(s + t)))
                        - 6
                        * math.log(-t) ** 2
                        * (-1j * math.pi * step(s + t) + math.log(abs(s + t)))
                        + 12
                        * (-1j * math.pi + math.log(s) - math.log(-t))
                        * (
                            math.pi**2 / 6
                            - (-math.log(s) + math.log(-t))
                            * (
                                1j * math.pi * step(-(s + t) / s)
                                - math.log(s)
                                + math.log(abs(s + t))
                            )
                            - mpmath.polylog(2, (s + t) / s)
                        )
                        + 12 * mpmath.polylog(3, -(1j * delta + t) / (1j * delta + s))
                        - 68 * mpmath.apery
                    )
                    / (6 * s * t),
                ],
            ],
            dtype=numpy.cdouble,
        )
        .view(numpy.double)
        .reshape(3, 4, 2)
    )

    return f.tolist()


def boundary_values(cfg):
    (domain_s_l, domain_s_h), (domain_t_l, domain_t_h) = cfg["system"]["input"][
        "boundaries"
    ]

    t = 0.5 * (domain_t_l + domain_t_h)
    s = 0.5 * (domain_s_l + domain_s_h)

    bcx = [[s, t]]
    bc = [[v, sol(v)] for v in bcx]

    with open(cfg["system"]["boundary_values"]["filename"], "w") as f:
        json.dump(bc, f)


def test_singular(x, atol):
    return (
        numpy.isclose(x[:, 0], 0, atol=atol)  # s = 0
        | numpy.isclose(x[:, 1], 0, atol=atol)  # t = 0
        | numpy.isclose(x[:, 0], -x[:, 1], atol=atol)  # s + t = 0
    )


def generate_phase_space(domain_low, domain_high, num_vars, num_points, atol=1e-1):
    rng = numpy.random.default_rng()

    x = rng.uniform(domain_low, domain_high, (num_points, num_vars))

    t = test_singular(x, atol)
    while t.any():
        x[t] = rng.uniform(domain_low, domain_high, (numpy.sum(t), num_vars))
        t = test_singular(x, atol)

    return x.tolist()


def validation(cfg):
    domain_low, domain_high = zip(*cfg["system"]["input"]["boundaries"])
    num_in = cfg["system"]["input"]["variables"]

    x = generate_phase_space(domain_low, domain_high, num_in, 4096)

    ff = [[v, sol(v)] for v in x]

    with open(cfg["validation"]["filename"], "w") as f:
        json.dump(ff, f)


def training(cfg):
    domain_low, domain_high = zip(*cfg["system"]["input"]["boundaries"])
    batch_size = cfg["hyperparameters"]["iterations"]["batch_size"]
    num_in = cfg["system"]["input"]["variables"]

    x = generate_phase_space(domain_low, domain_high, num_in, batch_size)

    with open(cfg["training"]["filename"], "w") as f:
        json.dump(x, f)


if __name__ == "__main__":
    cfg = config.open_config("box.json", generation=True)

    boundary_values(cfg)
    validation(cfg)
    training(cfg)
