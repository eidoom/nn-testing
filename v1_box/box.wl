(* ::Package:: *)

(* (5.381) *)
As={{0, 0, 0}, {0, -(eps/s), 0}, {(2*(-1 + 2*eps))/(s*t*(s + t)), (-2*(-1 + 2*eps))/(s^2*(s + t)), -((s + t + eps*t)/(s*(s + t)))}};
At={{-(eps/t), 0, 0}, {0, 0, 0}, {(-2*(-1 + 2*eps))/(t^2*(s + t)), (2*(-1 + 2*eps))/(s*t*(s + t)), -((s + eps*s + t)/(t*(s + t)))}};


Coefficient[As,eps,0]//MatrixForm
Coefficient[As,eps,1]//MatrixForm
Coefficient[At,eps,0]//MatrixForm
Coefficient[At,eps,1]//MatrixForm


(* (5.405) & (5.409) *)
h={
	(4 - (4*eps^2*Pi^2)/3 - 2*eps*Log[x] + eps^3*((7*Pi^2*Log[x])/6 + Log[x]^3/3 - Pi^2*Log[1 + x] - 
		Log[x]^2*Log[1 + x] - 2*Log[x]*PolyLog[2, -x] + 2*PolyLog[3, -x] - (34*Zeta[3])/3)),
	-1 + (eps^2*Pi^2)/12 + (7*eps^3*Zeta[3])/3,
	-1 + eps*Log[x] + eps^2*(Pi^2/12 - Log[x]^2/2) + eps^3*(-1/12*(Pi^2*Log[x]) + Log[x]^3/6 + (7*Zeta[3])/3)
};
h//MatrixForm


(* (5.403), x=t/s *)
f=(-s)^(-eps)*h/.{
	Log[x]->(Log[-t]-Log[-s]),
	Log[1+x]->(Log[-s-t]-Log[-s]),
	Nothing
}//Simplify;
f//MatrixForm


T={{0,0,eps/(c (-1+2 eps))},{0,eps/(c (-1+2 eps)),0},{1/(c s t),0,0}}/.c->1;
T//MatrixForm


(* (5.384) *)
Inverse[T]//MatrixForm


g=Series[T . f,{eps,0,3}]//Normal//Simplify;
g//MatrixForm


o=Table[
Coefficient[g[[i]],eps,j]//Simplify
,{i, 3}
,{j, 0, 3}
];


o


o[[3,4]]


analyticContinuation = {
	Log[-s]:>Log[s]-I*Pi,
	Log[-s-t]->Log[Abs[s+t]]-I*Pi*HeavisideTheta[s+t],
	PolyLog[2,-x]->-PolyLog[2,1+t/s]-(Log[-t]-Log[s])*(Log[Abs[s+t]]-Log[s]+I*Pi*HeavisideTheta[-1-t/s])+Zeta[2],
	PolyLog[3,-x]->PolyLog[3,-t/s]/.{t->t+I*delta,s->s+I*delta},
	Nothing
};


point = {s->3,t->-5};
(Log[-s] /. {t->t+I*delta,s->s+I*delta} /. delta->10^(-10)) - (Log[-s] /. analyticContinuation) /. point // N // Chop
(Log[-s-t] /. {t->t+I*delta,s->s+I*delta} /. delta->10^(-10)) - (Log[-s-t] /. analyticContinuation) /. point // N // Chop
(PolyLog[2,-t/s] /. {t->t+I*delta,s->s+I*delta} /. delta->10^(-10)) - (PolyLog[2,-t/s] /. analyticContinuation) /. point // N // Chop


o[[3,4]] /. analyticContinuation //Simplify //InputForm


(* EOF *)
