def de_matrix(v):
    # assume form A^0 + eps A^1
    # real valued
    # dimensions: input_variable, epsilon, function, function
    s = v[0]
    t = v[1]
    return [
        [
            [
                [0, 0, 0],
                [0, 0, 0],
                [-2 / (s * t * (s + t)), 2 / (s**2 * (s + t)), -1 / s],
            ],
            [
                [0, 0, 0],
                [0, -1 / s, 0],
                [4 / (s * t * (s + t)), -4 / (s**2 * (s + t)), -t / (s * (s + t))],
            ],
        ],
        [
            [
                [0, 0, 0],
                [0, 0, 0],
                [2 / (t**2 * (s + t)), -2 / (s * t * (s + t)), -1 / t],
            ],
            [
                [-1 / t, 0, 0],
                [0, 0, 0],
                [-4 / (t**2 * (s + t)), 4 / (s * t * (s + t)), -s / (t * (s + t))],
            ],
        ],
    ]
