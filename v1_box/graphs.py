import math
import matplotlib.pyplot, matplotlib.cm, numpy


def plot_validation_inference(xx, ff, nn, x_label, y_label, name):
    inf_plot_name = f"inference_{name}.pdf"
    print(f"Plotting {inf_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    # ax.set_yscale("log")

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    colours = numpy.concatenate(
        (
            matplotlib.cm.tab20(range(20)),
            matplotlib.cm.tab20b(range(0, 20, 2)),
            matplotlib.cm.tab20c(range(0, 20, 2)),
        )
    )
    c = 0

    for i, z in enumerate(zip(*[numpy.moveaxis(ll, 0, 2) for ll in (ff, nn)])):
        for j, (fij, nij) in enumerate(zip(*z)):
            fa = fij.real
            fb = fij.imag
            na = nij.real
            nb = nij.imag

            atol = 1e-2
            ta = not numpy.isclose(fa, 0.0, atol=atol).all()
            tb = not numpy.isclose(fb, 0.0, atol=atol).all()

            if ta:
                ax.plot(
                    xx,
                    fa,
                    label=f"$\mathrm{{Re}}\,f_{i}^{{({j})}}$",
                    linestyle="solid",
                    color=colours[2 * c],
                )
                ax.plot(
                    xx,
                    na,
                    linestyle="dashed",
                    color=colours[2 * c],
                )

            if tb:
                ax.plot(
                    xx,
                    fb,
                    label=f"$\mathrm{{Im}}\,f_{i}^{{({j})}}$",
                    linestyle="solid",
                    color=colours[2 * c + 1],
                )
                ax.plot(
                    xx,
                    nb,
                    linestyle="dashed",
                    color=colours[2 * c + 1],
                )

            if ta or tb:
                c += 1
                c %= 20

    ax.legend(bbox_to_anchor=(1, 0.5), loc="center left")

    fig.savefig(inf_plot_name, bbox_inches="tight")


def plot_validation_statistics(
    epochs, lrs, validation_metrics, validation_losses, training_losses, name
):
    val_plot_name = f"validation-{name}.pdf"
    print(f"Plotting {val_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")
    ax.set_yscale("log")

    ax.plot(epochs, lrs, label="Learning rate")
    ax.plot(epochs, validation_metrics, label="Validation metric")
    ax.plot(epochs, validation_losses, label="Validation loss")
    ax.plot(epochs, training_losses, label="Training loss")

    ax.legend()

    fig.savefig(val_plot_name, bbox_inches="tight")


def plot_validation_deviation(ff, nn, pcs):
    plotname = "deviation.pdf"
    print(f"Plotting {plotname}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
    )

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\log_{10}(NN/f)$")

    n = ff.shape[0]

    weights = numpy.full(n, 1 / n)

    mff = numpy.ma.masked_equal(ff, 0)

    r = numpy.log10(numpy.absolute(nn / mff))

    r = numpy.moveaxis(r, 0, 2)

    b = numpy.histogram_bin_edges(r.compressed(), bins="auto")

    for i, ri in enumerate(r):
        for j, rij in enumerate(ri):
            if rij.compressed().size:
                ax.hist(
                    rij,
                    bins=b,
                    histtype="step",
                    weights=weights,
                    label=f"$f_{i}^{{({j})}}$",
                )

    yl = ax.get_ylim()[1] * 0.9
    for pc in pcs:
        xl = math.log10(1 + pc / 100)
        ax.axvline(xl, color="black", linewidth=1)
        ax.axvline(math.log10(1 - pc / 100), color="black", linewidth=1)

        ax.text(
            xl,
            yl,
            f"${pc}\%$",
            horizontalalignment="left",
            verticalalignment="center",
            # transform=ax.transAxes,
        )

    ax.legend()

    fig.savefig(plotname, bbox_inches="tight")
