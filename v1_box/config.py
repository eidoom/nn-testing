import pathlib, random, json, importlib
import torch


def open_config(filename, generation=False):
    with open(filename, "r") as f:
        config = json.load(f)

    config["device"] = torch.device(config["device"])

    config["model_filename"] = pathlib.Path(config["model_filename"])
    config["system"]["de_matrix"]["filename"] = pathlib.Path(
        config["system"]["de_matrix"]["filename"]
    )
    config["system"]["boundary_values"]["filename"] = pathlib.Path(
        config["system"]["boundary_values"]["filename"]
    )
    config["training"]["filename"] = pathlib.Path(config["training"]["filename"])
    config["validation"]["filename"] = pathlib.Path(config["validation"]["filename"])

    num_in = config["system"]["input"]["variables"]
    num_func = config["system"]["output"]["functions"]

    d = config["system"]["de_matrix"]["filename"]

    if not d.is_file():
        raise Exception(f"DE matrix file '{d}' not found")

    mod = importlib.import_module(d.stem)

    config["system"]["de_matrix"]["function"] = mod.de_matrix

    m = mod.de_matrix([random.random() for _ in range(num_in)])
    assert len(m) == num_in
    for x in m:
        assert len(x) == 2
        for i in x:
            assert len(i) == num_func
            for j in i:
                assert len(j) == num_func

    if not generation:
        bv = config["system"]["boundary_values"]["filename"]

        if not bv.is_file():
            raise Exception(f"Boundary values file '{bv}' not found")

        with open(bv, "r") as f:
            (
                config["system"]["boundary_values"]["input"],
                config["system"]["boundary_values"]["output"],
            ) = zip(*json.load(f))

        bx = config["system"]["boundary_values"]["input"]
        by = config["system"]["boundary_values"]["output"]
        num_eps = config["system"]["output"]["epsilon_orders"]

        assert len(bx) == len(by)
        for x, y in zip(bx, by):
            assert len(x) == num_in
            assert len(y) == num_func
            for f in y:
                assert len(f) == num_eps
                for c in f:
                    assert len(c) == 2

        v = config["validation"]["filename"]

        if not v.is_file():
            raise Exception(f"Validation dataset file '{v}' not found")

        with open(v, "r") as f:
            (
                config["validation"]["input"],
                config["validation"]["output"],
            ) = zip(*json.load(f))

        vx = config["system"]["boundary_values"]["input"]
        vy = config["system"]["boundary_values"]["output"]

        assert len(vx) == len(vy)
        for x, y in zip(vx, vy):
            assert len(x) == num_in
            assert len(y) == num_func
            for f in y:
                assert len(f) == num_eps
                for c in f:
                    assert len(c) == 2

        t = config["training"]["filename"]

        if not t.is_file():
            print(f"Warning: no training input values file '{t}' found")

        with open(t, "r") as f:
            config["training"]["input"] = json.load(f)

        tx = config["training"]["input"]
        batch_size = config["hyperparameters"]["iterations"]["batch_size"]

        assert len(tx) == batch_size
        for x in tx:
            assert len(x) == num_in

    return config
