#!/usr/bin/env python3

import time, argparse, pathlib, math
import torch, numpy
import config, graphs
from model import FeynmanIntegralsModel


class DifferentialEquationsLoss(torch.nn.Module):
    def __init__(self, x, y, boundary_bias, model, device):
        super().__init__()
        self.bvx = torch.tensor(x, device=device, dtype=model.dtype_real)
        self.bvy = torch.tensor(y, device=device, dtype=model.dtype_real).view(
            len(x), model.num_func * model.num_eps * 2
        )
        self.boundary_bias = boundary_bias
        self.model = model
        self.device = device
        self.loss = torch.nn.MSELoss()

    def forward(self, x, mats, pred, training=True):
        batch_size = len(x)

        dnn = (
            torch.stack(
                [
                    torch.autograd.grad(
                        outputs=pred[:, i],
                        inputs=x,
                        grad_outputs=torch.ones(
                            batch_size, device=self.device, dtype=self.model.dtype_real
                        ),
                        retain_graph=training or i != self.model.output_size - 1,
                        create_graph=training,
                    )[0]
                    for i in range(self.model.output_size)
                ]
            ).view(
                self.model.num_func,
                self.model.num_eps,
                2,
                batch_size,
                self.model.num_in,
            )
            # ^ size = (num_func, num_eps, 2(complex), batch_size, num_in)
            .permute((1, 3, 4, 2, 0))
            # ^ size = (num_eps, batch_size, num_in, 2(complex), num_func)
        )

        pred = (
            pred
            # ^ size = (batch_size, num_func * num_eps * 2(complex))
            .view(batch_size, self.model.num_func, self.model.num_eps, 2)
            # ^ size = (batch_size, num_func, num_eps, 2(complex))
            .permute((2, 0, 3, 1))
            # ^ size = (num_eps, batch_size, 2(complex), num_func)
            .repeat_interleave(repeats=self.model.num_in, dim=1)  # for each d/dxi
            # TODO sure ^ is right? alt: .repeat((1, self.model.num_in, 1, 1))  # for each d/dxi
            # ^ size = (num_eps, batch_size * num_in, 2(complex), num_func)
            .view(
                self.model.num_eps,
                batch_size * self.model.num_in * 2,
                self.model.num_func,
                1,
            )
            # ^ size = (num_eps, batch_size * num_in * 2(complex), num_func, 1)
        )

        # these broadcast over batch+input_vars+complex dimensions
        target = torch.empty(
            (
                self.model.num_eps,
                batch_size * self.model.num_in * 2,
                self.model.num_func,
                1,
            ),
            device=self.device,
            dtype=self.model.dtype_real,
        )

        target[0] = torch.matmul(mats[0], pred[0])

        for k in range(1, self.model.num_eps):
            target[k] = torch.matmul(mats[0], pred[k]) + torch.matmul(
                mats[1], pred[k - 1]
            )

        target = target.view(
            self.model.num_eps, batch_size, self.model.num_in, 2, self.model.num_func
        )

        bulk = self.loss(dnn, target)

        bv = self.model(self.bvx)
        boundary = self.loss(bv, self.bvy)

        return bulk + self.boundary_bias * boundary


def mats_num_minibatch(df, xs, num_in, num_func, device, dtype):
    return (
        torch.stack([torch.tensor(df(x), device=device, dtype=dtype) for x in xs])
        # ^ size = (batch_size, num_in, 2(eps), num_func, num_func)
        .permute((2, 0, 1, 3, 4))
        # ^ size = (2(eps), batch_size, num_in, num_func, num_func)
        .repeat_interleave(repeats=2, dim=2)
        # ^ size = (2(eps), batch_size, num_in * 2(complex), num_func, num_func)
        .contiguous()
    )


def mats_num_batch(df, xs, num_in, num_func, device, dtype):
    return (
        mats_num_minibatch(df, xs, num_in, num_func, device, dtype)
        # ^ size = (2(eps), batch_size, num_in * 2(complex), num_func, num_func)
        .view(2, len(xs) * num_in * 2, num_func, num_func)
        # ^ size = (2(eps), batch_size * num_in * 2(complex), num_func, num_func)
    )


def train(model, x_train, loss_fn, mats_train, optimiser):
    # Compute prediction error
    pred_train = model(x_train)
    loss_train = loss_fn(x_train, mats_train, pred_train)

    # Backpropagation
    optimiser.zero_grad()
    loss_train.backward()
    optimiser.step()

    return loss_train


# def standardise(x):
#     x_mean = torch.mean(x, dim=0)
#     x_std = torch.std(x, dim=0)


class PhaseSpaceDataset(torch.utils.data.TensorDataset):
    def __getitem__(self, index):
        return tuple(tensor[index] for tensor in self.tensors), index


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Training script")
    parser.add_argument(
        "config_file", type=pathlib.Path, help="Path to JSON configuration file."
    )
    args = parser.parse_args()

    print("Initialising")
    start = time.time()

    cfg = config.open_config(args.config_file)

    device = cfg["device"]
    model_name = cfg["model_filename"]
    hidden_layers = cfg["hyperparameters"]["architecture"]["hidden_layers"]
    learning_rate = cfg["hyperparameters"]["optimiser"]["initial_learning_rate"]
    weight_decay = cfg["hyperparameters"]["optimiser"]["weight_decay"]
    lr_factor = cfg["hyperparameters"]["learning_rate_scheduler"]["factor"]
    lr_patience = cfg["hyperparameters"]["learning_rate_scheduler"]["patience"]
    lr_threshold = cfg["hyperparameters"]["learning_rate_scheduler"]["threshold"]
    batch_size = cfg["hyperparameters"]["iterations"]["batch_size"]
    minibatch_size = cfg["hyperparameters"]["iterations"]["minibatch_size"]
    number_minibatches = batch_size // minibatch_size  # TODO check alignment
    epochs = cfg["hyperparameters"]["iterations"]["max_epochs"]
    target = cfg["hyperparameters"]["target"]["accuracy"]
    patience = cfg["hyperparameters"]["target"]["patience"]
    boundary_bias = cfg["hyperparameters"]["loss_function"]["boundary_bias"]
    num_in = cfg["system"]["input"]["variables"]
    num_eps = cfg["system"]["output"]["epsilon_orders"]
    num_func = cfg["system"]["output"]["functions"]
    bvx = cfg["system"]["boundary_values"]["input"]
    bvy = cfg["system"]["boundary_values"]["output"]
    vx = cfg["validation"]["input"]
    vy = cfg["validation"]["output"]
    tx = cfg["training"]["input"]
    de_matrix = cfg["system"]["de_matrix"]["function"]

    model = FeynmanIntegralsModel(num_in, hidden_layers, num_eps, num_func, device)

    loss_fn = DifferentialEquationsLoss(
        x=bvx,
        y=bvy,
        boundary_bias=boundary_bias,
        model=model,
        device=device,
    )

    # https://pytorch.org/docs/stable/optim.html#algorithms
    optimiser = torch.optim.AdamW(
        params=model.parameters(),
        lr=learning_rate,
        betas=(0.9, 0.999),
        eps=1e-8,
        weight_decay=weight_decay,
        fused=device == torch.device("cuda"),
    )

    # https://pytorch.org/docs/stable/optim.html#how-to-adjust-learning-rate
    scheduler_dynamic = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimiser,
        factor=lr_factor,
        patience=lr_patience,
        threshold=lr_threshold,
        verbose=True,
    )

    metric = torch.nn.MSELoss()

    # grad required for validation_metrics loss
    x = torch.tensor(
        vx,
        device=device,
        dtype=model.dtype_real,
        requires_grad=True,
    )

    f = torch.tensor(vy, device=device, dtype=model.dtype_real)

    mats = mats_num_batch(
        de_matrix, x, num_in, num_func, device=device, dtype=model.dtype_real
    )

    x_train = torch.tensor(
        tx,
        device=device,
        dtype=model.dtype_real,
        requires_grad=True,
    )

    mats_train_file = pathlib.Path("de_matrix_train.pt")
    if mats_train_file.is_file():
        print(f"Loading cache {mats_train_file}")
        mats_train = torch.load(mats_train_file)
    else:
        mats_train = mats_num_minibatch(
            de_matrix,
            x_train,
            num_in,
            num_func,
            device=device,
            dtype=model.dtype_real,
        )
        print(f"Saving cache {mats_train_file}")
        torch.save(mats_train, "de_matrix_train.pt")

    dataloader_train = torch.utils.data.DataLoader(
        PhaseSpaceDataset(x_train),
        batch_size=minibatch_size,
        shuffle=True,
    )

    dur = time.time() - start
    print(f"Initialisation took {dur:.1f}s")

    print("Training")
    start = time.time()

    counter = 0

    lrs = []
    validation_metrics = []
    validation_losses = []
    training_losses = []
    for t in range(1, epochs + 1):
        epoch_start = time.time()

        lrs.append(optimiser.param_groups[-1]["lr"])
        print(
            f"Epoch {t}/{epochs} (lr: {lrs[-1]:.4f})\n-------------------------------\nTraining loss:"
        )

        model.train()
        loss_epoch = 0
        for minibatch, ((x_minibatch,), indices) in enumerate(dataloader_train):
            mats_minibatch = mats_train[:, indices].view(
                2, minibatch_size * num_in * 2, num_func, num_func
            )

            loss_train = train(model, x_minibatch, loss_fn, mats_minibatch, optimiser)
            loss_epoch += loss_train.detach().cpu()

            if math.fmod(minibatch, number_minibatches / 10) < 1:
                print(f"{100*minibatch/number_minibatches:2.0f}%: {loss_train:10.7f}")

        model.eval()
        pred_valid = model(x)
        loss_valid = loss_fn(x, mats, pred_valid, training=False).detach().cpu()
        metric_valid = (
            metric(
                pred_valid.detach().view(len(x), model.num_func, model.num_eps, 2), f
            )
            .detach()
            .cpu()
        )

        print(f"validation metric: {metric_valid:.7f}")
        print(f"             loss: {loss_valid:.7f}")
        print(f"       epoch took: {time.time() - epoch_start:.2f}s")

        validation_metrics.append(metric_valid)
        validation_losses.append(loss_valid)
        training_losses.append(loss_epoch / number_minibatches)

        if metric_valid < target:
            counter += 1
            if counter >= patience:
                epochs = t
                print("Validation metric has met target. Stopping training.")
                break
        else:
            counter = 0

        scheduler_dynamic.step(metric_valid)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    print(f"Saving model {model_name}")
    torch.save(model.state_dict(), model_name)

    print("Saving validation statistics")
    validation_statistics = numpy.stack(
        (
            numpy.arange(1, epochs + 1),
            lrs,
            validation_metrics,
            validation_losses,
            training_losses,
        )
    )

    numpy.save("validation-statistics.npy", validation_statistics)
