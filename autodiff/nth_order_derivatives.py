#!/usr/bin/env python3
"""
https://sidsite.com/posts/autodiff/
"""

import math
from collections import defaultdict


def sin(a):
    value = math.sin(a.value)
    local_gradients = ((a, lambda d: d * cos(a)),)
    return Var(value, local_gradients)


def cos(a):
    value = math.cos(a.value)
    local_gradients = ((a, lambda d: d * -sin(a)),)
    return Var(value, local_gradients)


def exp(a):
    value = math.exp(a.value)
    local_gradients = ((a, lambda d: d * exp(a)),)
    return Var(value, local_gradients)


def log(a):
    value = math.log(a.value)
    local_gradients = ((a, lambda d: d / a),)
    return Var(value, local_gradients)


def sqrt(a):
    value = math.sqrt(a.value)
    local_gradients = ((a, lambda d: d / (Var(2) * sqrt(a))),)
    return Var(value, local_gradients)


class Var:
    def __init__(self, value, local_gradients=()):
        self.value = value
        self.local_gradients = local_gradients

    def __repr__(self):
        return str(self.value)

    def __add__(self, other):
        value = self.value + other.value
        local_gradients = (
            (self, lambda d: d),  # local gradient is 1, so multiply path_value by 1
            (other, lambda d: d),  # local gradient is 1, so multiply path_value by 1
        )
        return Var(value, local_gradients)

    def __sub__(self, other):
        value = self.value - other.value
        local_gradients = (
            (self, lambda d: d),
            (other, lambda d: -d),
        )
        return Var(value, local_gradients)

    def __mul__(self, other):
        value = self.value * other.value
        local_gradients = (
            (self, lambda d: d * other),
            (other, lambda d: d * self),
        )
        return Var(value, local_gradients)

    def __truediv__(self, other):
        value = self.value / other.value
        local_gradients = (
            (self, lambda d: d / other),
            (other, lambda d: -d * self / (other * other)),
        )
        return Var(value, local_gradients)

    def __pow__(self, other):
        value = self.value**other.value
        local_gradients = (
            (self, lambda d: d * other * (self ** (other - Var(1)))),
            (other, lambda d: d * log(self) * (self**other)),
        )
        return Var(value, local_gradients)

    def __neg__(self):
        value = -self.value
        local_gradients = ((self, lambda d: -d),)
        return Var(value, local_gradients)

    # don't define == because then __hash__ needs defined too
    # also equality is for the graph node, not its value, unlike comparisons

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.value <= other.value

    def __gt__(self, other):
        return self.value > other.value

    def __ge__(self, other):
        return self.value >= other.value

    def grad(self):
        """
        Compute the first derivatives of self wrt child variables.
        This uses backward/reverse-mode autodiff.
        """
        gradients = defaultdict(lambda: Var(0))

        # recurse through chain rule
        # note local gradients must be functions so their evaluation is delayed to avoid infinite recursion
        def compute_gradients(variable, path_value):
            for child_variable, loc_grad_factor in variable.local_gradients:
                path_to_child_value = loc_grad_factor(path_value)
                gradients[child_variable] += path_to_child_value
                compute_gradients(child_variable, path_to_child_value)

        # path_value=1 is from self differentiated wrt itself
        compute_gradients(self, path_value=Var(1))

        return dict(gradients)


def eg1():
    """
    A 2nd derivative example.
    y = x*x = x**2
    y' = 2x (= 2*3 = 6)
    y'' = 2
    """

    x = Var(3)
    y = x * x

    dy_by_dx = y.grad()[x]

    print("y =", y)
    print("The derivative of y wrt x =", dy_by_dx)

    dy_by_dx2 = dy_by_dx.grad()[x]
    print("The 2nd derivative of y wrt x =", dy_by_dx2)

    print("---")


def eg2():
    """
    2nd derivative of our original example.
    d = a*(a + b)
    d = a**2 + ab
    dd_by_da = 2a + b (= 2*4 + 3 = 11)
    dd_by_da2 = 2
    """

    a = Var(4)
    b = Var(3)
    c = a + b  # = 4 + 3 = 7
    d = a * c  # = 4 * 7 = 28

    dd_by_da = d.grad()[a]

    print("d =", d)
    print("The partial derivative of d wrt a =", dd_by_da)

    dd_by_da2 = dd_by_da.grad()[a]
    print("The 2nd partial derivative of d wrt a =", dd_by_da2)

    print("---")


def eg3():
    """
    Another second derivative example, with division.
    y = a*a/b
    dy_by_da = 2*a/b (=2*3/7=0.8571...)
    dy_by_da2 = 2/b (=2/7=0.2857...)
    """

    a = Var(3)
    b = Var(7)
    y = a * a / b

    dy_by_da = y.grad()[a]

    print("y =", y)
    print("The partial derivative of y wrt a =", dy_by_da)

    dy_by_da2 = dy_by_da.grad()[a]
    print("The 2nd partial derivative of y wrt a =", dy_by_da2)

    print("---")


def eg4():
    """
    Nth derivative example
    y = x**N
    y' = N*x**(N-1) (=10*3**9=196830)
    y'' = (N-1)*N*x**(N-2) (=9*10*3**8=590490)
    y''' = (N-2)(N-1)*N*x**(N-3) (=8*9*10*3**7=1574640)
    etc.
    """

    N = 4  # RecursionError for N > 4
    x = Var(3)

    y = Var(1)
    for i in range(10):
        y *= x
    print("y =", y)

    target = y
    for n in range(1, N + 1):
        target = target.grad()[x]
        print(f"The n={n} derivative of y wrt x is", target)

    print("---")


def eg5():
    x = Var(2)

    y = x * x * x
    print(2**3 == y.value)

    dy = y.grad()[x]
    print(3 * 2**2 == dy.value)

    d2y = dy.grad()[x]
    print(3 * 2 * 2 == d2y.value)

    d3y = d2y.grad()[x]
    print(3 * 2 == d3y.value)

    print(x not in d3y.grad())

    print("---")


def eg6():
    x = Var(2)

    y = sin(x)
    print(math.sin(2) == y.value)

    dy = y.grad()[x]
    print(math.cos(2) == dy.value)

    d2y = dy.grad()[x]
    print(-math.sin(2) == d2y.value)

    d3y = d2y.grad()[x]
    print(-math.cos(2) == d3y.value)

    d4y = d3y.grad()[x]
    print(math.sin(2) == d4y.value)

    print("---")


def eg7():
    x = Var(2)

    y = exp(x)

    dy = y.grad()[x]
    print(y.value == dy.value)

    d2y = dy.grad()[x]
    print(y.value == d2y.value)

    d3y = dy.grad()[x]
    print(y.value == d3y.value)

    print("---")


def eg8():
    def f(a, b):
        return a**b

    a = Var(3)
    b = Var(-2)
    delta = Var(1e-8)

    y = f(a, b)

    alg_dy = y.grad()[b]
    num_dy = (f(a, b + delta) - f(a, b)) / delta
    print(alg_dy - num_dy < delta)

    alg_d2y = alg_dy.grad()[b]
    num_d2y = (
        f(a, b + Var(2) * delta) - Var(2) * f(a, b + delta) + f(a, b)
    ) / delta ** Var(2)
    print(alg_d2y - num_d2y < delta)

    print("---")


if __name__ == "__main__":
    eg1()
    eg2()
    eg3()
    eg4()
    eg5()
    eg6()
    eg7()
    eg8()
