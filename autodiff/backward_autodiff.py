#!/usr/bin/env python3
"""
https://sidsite.com/posts/autodiff/
"""

import math
from collections import defaultdict


def sin(a):
    value = math.sin(a.value)
    local_gradients = ((a, math.cos(a.value)),)
    return Var(value, local_gradients)


def exp(a):
    value = math.exp(a.value)
    local_gradients = ((a, value),)
    return Var(value, local_gradients)


def log(a):
    value = math.log(a.value)
    local_gradients = ((a, 1 / a.value),)
    return Var(value, local_gradients)


class Var:
    def __init__(self, value, local_gradients=()):
        self.value = value
        self.local_gradients = local_gradients

    def __repr__(self):
        return str(self.value)

    def __add__(self, other):
        value = self.value + other.value
        local_gradients = (
            (self, 1),  # the local derivative with respect to self is 1
            (other, 1),  # the local derivative with respect to other is 1
        )
        return Var(value, local_gradients)

    def __mul__(self, other):
        value = self.value * other.value
        local_gradients = ((self, other.value), (other, self.value))
        return Var(value, local_gradients)

    def __neg__(self):
        value = -self.value
        local_gradients = ((self, -1),)
        return Var(value, local_gradients)

    def inv(self):
        value = 1 / self.value
        local_gradients = ((self, -1 / self.value**2),)
        return Var(value, local_gradients)

    def __sub__(self, other):
        return self + (-other)

    def __truediv__(self, other):
        return self * other.inv()

    def grad(self):
        """Compute the first derivatives of self
        with respect to child variables.
        """
        gradients = defaultdict(lambda: 0)

        # recurse through chain rule
        def compute_gradients(variable, path_value):
            for child_variable, local_gradient in variable.local_gradients:
                value_of_path_to_child = path_value * local_gradient
                gradients[child_variable] += value_of_path_to_child
                compute_gradients(child_variable, value_of_path_to_child)

        # path_value=1 is from self differentiated wrt itself
        compute_gradients(self, path_value=1)

        return dict(gradients)


if __name__ == "__main__":
    # 1
    a = Var(4)
    b = Var(3)
    c = a + b  # = 4 + 3 = 7
    d = a * c  # = 4 * 7 = 28
    grads = d.grad()

    print(d)
    print(grads[a])
    print(grads[b])
    print(grads[c])
    print()

    # 2
    a = Var(230.3)
    b = Var(33.2)
    y = (a / b - a) * (b / a + a + b) * (a - b)
    grads = y.grad()

    print(y)
    print(grads[a])
    print(grads[b])
    print()

    # 3
    def f(a, b, c):
        f = sin(a * b) + exp(c - (a / b))
        return log(f * f) * c

    a = Var(43)
    b = Var(3)
    c = Var(2)
    y = f(a, b, c)
    grads = y.grad()

    print(y)
    print(grads[a])
    print(grads[b])
    print(grads[c])
    print()
