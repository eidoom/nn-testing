#!/usr/bin/env python3
"""
https://vmartin.fr/understanding-automatic-differentiation-in-30-lines-of-python.html
"""

import operator, math


class Leaves:
    def __init__(self, op, *values):
        self.op = op
        self.values = values


class Var:
    def __init__(self, value=None, leaves=None):
        self.leaves = leaves
        self.value = value

        if value is None and leaves is not None:
            self.forward()

    def forward(self):
        """
        compute forward pass of leaves in the tree and update value
        """
        self.value = self.leaves.op(*(leaf.value for leaf in self.leaves.values))

    def grad(self, wrt):
        # Derivative of a tensor with itself is 1
        if self is wrt:
            return Var(1)

        # Derivative of a scalar with another tensor is 0
        if self.leaves is None:
            return Var(0)

        op = self.leaves.op

        if op == operator.add:  # (a + b)' = a' + b'
            a, b = self.leaves.values
            t = a.grad(wrt) + b.grad(wrt)
        elif op == operator.sub:  # (a - b)' = a' - b'
            a, b = self.leaves.values
            t = a.grad(wrt) - b.grad(wrt)
        elif op == operator.mul:  # (ab)' = a'b + ab'
            a, b = self.leaves.values
            t = a.grad(wrt) * b + a * b.grad(wrt)
        elif op == operator.truediv:  # (ab)' = (a'b - ab') / b²
            a, b = self.leaves.values
            t = (a.grad(wrt) * b - a * b.grad(wrt)) / (b * b)
        elif op == operator.neg:  # (-a)' = -a'
            (a,) = self.leaves.values
            t = -a.grad(wrt)
        elif op is math.exp:  # exp(a)' = a'exp(a)
            (a,) = self.leaves.values
            t = a.grad(wrt) * a.exp()
        elif op is math.log:  # ln(a)' = a'/a
            (a,) = self.leaves.values
            t = a.grad(wrt) / a
        else:
            raise NotImplementedError(f"This op is not implemented. {op}")

        return t

    def __repr__(self):
        return str(self.value)

    def __add__(self, other):
        return Var(leaves=Leaves(operator.add, self, other))

    def __sub__(self, other):
        return Var(leaves=Leaves(operator.sub, self, other))

    def __mul__(self, other):
        return Var(leaves=Leaves(operator.mul, self, other))

    def __truediv__(self, other):
        return Var(leaves=Leaves(operator.truediv, self, other))

    def __neg__(self):
        return Var(leaves=Leaves(operator.neg, self))

    def exp(self):
        return Var(leaves=Leaves(math.exp, self))

    def log(self):
        return Var(leaves=Leaves(math.log, self))


if __name__ == "__main__":
    x = Var(3)
    y = Var(5)

    z = (Var(12) - (x * y.exp())) / (Var(45) + x * y * (-x).exp())
    print(z)
    print(z.grad(x), -3.347297773010692)
    print(z.grad(y), -9.70176956641438)

    a = (-z).log()
    print(a)
    print(a.grad(x), 0.35344928140701615)
    print(a.grad(y), 1.0244333531585895)
