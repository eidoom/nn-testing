#!/usr/bin/env python3

import math

import torch, numpy, scipy

from ode import (
    NeuralNetwork,
    MyLoss,
    inference,
    init_torch,
    training,
    plot_validation,
    plot_inference,
)


def li1(z):
    # singularity at z = 1
    # analytic for z < 1
    # branch cut for z > 1
    return -numpy.log(1.0 - z)


def li2(z):
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.spence.html#scipy.special.spence
    # alt: https://www.mpmath.org/doc/current/functions/zeta.html#polylog
    return scipy.special.spence(1.0 - z)


if __name__ == "__main__":
    print("Initialising")

    # hyperparameters
    learning_rate = 1e-2
    decay = 0.9
    batch_size = 100
    number_batches = 100
    epochs = 5
    test_size = 1000

    domain_l = -1.0 - 2.0 * math.pi
    domain_r = -1.0

    device = init_torch()

    model = NeuralNetwork(layers=[1] + [128] * 3 + [1]).to(device)

    ff = lambda z: li2(z) + li1(z)

    loss_fn = MyLoss(
        df=lambda z: -torch.log(1.0 - z) / z + 1.0 / (1.0 - z),
        x=[-1.0],
        y=[ff(-1.0)],
        model=model,
        device=device,
    )

    x_ = numpy.linspace(
        start=domain_l,
        stop=domain_r,
        num=test_size,
        endpoint=False,
    )

    x = torch.reshape(
        torch.tensor(
            x_,
            device=device,
            requires_grad=True,
            dtype=torch.float32,
        ),
        (test_size, 1),
    )  # grad required for validation_metrics loss

    f_ = ff(x_)
    f = torch.tensor(f_, device=device, dtype=torch.float32)

    lrs, validation_metrics, validation_losses = training(
        loss_fn,
        model,
        device,
        learning_rate,
        decay,
        domain_l,
        domain_r,
        batch_size,
        number_batches,
        epochs,
        x,
        f,
    )

    x = x.detach()
    nn = inference(model, x)

    nn = nn.cpu().numpy()

    print("Plotting")

    plot_validation(
        lrs, validation_metrics, validation_losses, name="validation-nonlinear.pdf"
    )

    plot_inference(x_, f_, nn, name="inference-nonlinear.pdf")

    # plot_deviation(f_, nn, name="deviation-nonlinear.pdf")

    print("Saving model")
    torch.save(model.state_dict(), "model-nonlinear.pth")

    print("Done!")
