import numpy
import system as s
from phasespace import get_regions

test_size = 1000

xx = numpy.concatenate(
    [
        numpy.linspace(
            start=region["start"],
            stop=region["end"],
            num=region["number"],
            dtype=numpy.cdouble,
        )
        for region in get_regions(test_size)
    ]
)

# ff = numpy.stack((numpy.ones_like(xx), s.li1(xx), s.li2(xx)), axis=1)
# ff = numpy.stack((numpy.ones_like(xx).real, s.li1(xx).real, s.li2(xx).real), axis=1)
ff = numpy.stack((numpy.ones_like(xx).imag, s.li1(xx).imag, s.li2(xx).imag), axis=1)
xx = xx.real
