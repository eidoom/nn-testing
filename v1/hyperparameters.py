import torch

import system as s

learning_rate = 1e-2
decay = 0.9

batch_size = 100
number_batches = 100
epochs = 10

dtype = torch.float32

layers = [len(s.bcx)] + [20] * 2 + [len(s.bcy)]
