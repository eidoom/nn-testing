#!/usr/bin/env python3

import time, math, random
import torch, numpy, matplotlib.pyplot, matplotlib.ticker, scipy.special

import system as s, hyperparameters as h, validation as v, phasespace as p


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers):
        super().__init__()

        self.stack = torch.nn.Sequential(
            *[
                f(x)
                for x in zip(layers[:-2], layers[1:-1])
                for f in (
                    lambda x: torch.nn.Linear(*x),
                    lambda _: torch.nn.Tanh(),
                )
            ],
            torch.nn.Linear(layers[-2], layers[-1]),
        )

    def forward(self, x):
        return self.stack(x)


class MyLoss(torch.nn.Module):
    def __init__(self, df, x, y, model, device, dtype=torch.float32):
        super().__init__()
        self.df = lambda v: torch.tensor(df(v), device=device, dtype=dtype)
        self.bcx = torch.tensor(x, device=device, dtype=dtype)
        self.bcy = torch.tensor(y, device=device, dtype=dtype)
        self.model = model
        self.device = device

        # check dims
        assert self.bcx.size(dim=0) == 1
        tmp_mat = self.df([random.random()]).size()
        assert len(tmp_mat) == 2
        num = self.bcy.size(dim=0)
        for ax in tmp_mat:
            assert ax == num

    def forward(self, x, pred, training=True):
        # input_dim = 1
        batch_dim = x.size(dim=0)
        output_dim = pred.size(dim=1)

        loss = torch.nn.MSELoss()

        # loop over outputs of NN
        dnn = torch.stack(
            [
                torch.autograd.grad(
                    outputs=pred[:, i],
                    inputs=x,
                    grad_outputs=torch.ones(batch_dim, device=self.device),
                    retain_graph=training or i != output_dim - 1,
                    create_graph=training,
                )[0]
                for i in range(output_dim)
            ]
        ).transpose(0, 1)

        mats = torch.stack([self.df(xx) for xx in x])

        # transpose pred from row [dim=(batch dim, nn output dim)] to vector [dim=(batch dim, nn output dim, 1)]
        pred = torch.unsqueeze(pred, dim=2)

        # this broadcasts over batch dimension
        target = torch.matmul(mats, pred)

        bulk = loss(dnn, target)

        bv = self.model(self.bcx)
        boundary = loss(bv, self.bcy)

        return bulk + boundary


if __name__ == "__main__":
    print("Initialising")

    torch.set_default_dtype(h.dtype)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    model = NeuralNetwork(layers=h.layers).to(device)

    loss_fn = MyLoss(
        df=s.ode_matrix,
        x=s.bcx,
        y=s.bcy,
        model=model,
        device=device,
        dtype=h.dtype,
    )

    optimiser = torch.optim.Adam(model.parameters(), lr=h.learning_rate)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=h.decay)

    x = torch.unsqueeze(
        torch.tensor(
            v.xx,
            device=device,
            requires_grad=True,
            dtype=h.dtype,
        ),
        dim=1,
    )  # grad required for validation_metrics loss

    f = torch.tensor(v.ff, device=device, dtype=h.dtype)

    print("Training")
    print(f"Batch size: {h.batch_size}")
    start = time.time()

    regions = p.get_regions(h.batch_size)
    dists = [torch.distributions.uniform.Uniform(r["start"], r["end"]) for r in regions]

    lrs = []
    validation_metrics = []
    validation_losses = []
    for t in range(1, 1 + h.epochs):
        lrs.append(scheduler.get_last_lr())
        print(
            f"Epoch {t}/{h.epochs} (lr: {lrs[-1][0]:.4f})\n-------------------------------"
        )

        model.train()
        for batch in range(h.number_batches):
            # Compute prediction error
            x_train = (
                torch.concatenate(
                    [dist.sample((r["number"], 1)) for dist, r in zip(dists, regions)]
                )
                .to(device)
                .requires_grad_()
            )

            pred = model(x_train)
            loss = loss_fn(x_train, pred)

            # Backpropagation
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()

            if not batch % (h.number_batches // 10):
                print(f"loss: {loss:9.6f}  [{batch:>3d}/{h.number_batches:>4d}]")

        model.eval()
        pred = model(x)
        validation_loss = loss_fn(x, pred, training=False).detach()
        validation_metric = torch.nn.L1Loss()(pred.detach(), f)
        print(f"validation metric: {validation_metric:.6f}")
        print(f"             loss: {validation_loss:.6f}")

        validation_metrics.append(validation_metric.cpu())
        validation_losses.append(validation_loss.cpu())
        scheduler.step()

    lrs = numpy.array(lrs)
    validation_metrics = numpy.array(validation_metrics)
    validation_losses = numpy.array(validation_losses)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    # validation statistics plot
    val_plot_name = "validation-log.pdf"
    print(f"Plotting {val_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.set_yscale("log")
    ax.plot(lrs, label="Learning rate")
    ax.plot(validation_metrics, label="Validation metric")
    ax.plot(validation_losses, label="Validation loss")
    ax.legend()
    fig.savefig(val_plot_name)

    # validation function plot
    inf_plot_name = "inference.pdf"
    print(f"Plotting {inf_plot_name}")

    model.eval()
    x = x.detach()
    with torch.no_grad():
        nn = model(x)
    nn = nn.cpu().numpy()

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    colours = matplotlib.cm.tab10(range(len(s.bcy)))
    for i, (fff, c) in enumerate(zip(v.ff.T, colours)):
        ax.plot(v.xx, fff, label=f"f{i} truth", linestyle="solid", color=c)
    for i, (nnn, c) in enumerate(zip(nn.T, colours)):
        ax.plot(v.xx, nnn, label=f"f{i} surrogate", linestyle="dashed", color=c)
    ax.legend(bbox_to_anchor=(1, 0.5), loc="center left")
    fig.savefig(inf_plot_name, bbox_inches="tight")

    model_name = "model.pth"
    print(f"Saving model {model_name}")
    torch.save(model.state_dict(), model_name)
