import math
import numpy, scipy.special


def li1(z):
    # singularity at z = 1
    # analytic for z < 1
    # branch cut for z > 1
    return -numpy.log(1.0 - z)


def li2(z):
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.spence.html#scipy.special.spence
    # alt: https://www.mpmath.org/doc/current/functions/zeta.html#polylog
    return scipy.special.spence(1.0 - z)


cut = 0.1
poles = [0.0, 1.0]  # poles in f and df

# # analytic region, solution is real
# domain_r = 1.0
# domain_l = domain_r - 2.0 * math.pi

# x = -1.0
# bcx = [x]
# bcy = [1.0, li1(x), li2(x)]


# complex solution
domain_l = 1.0
domain_r = domain_l + math.pi

x = 1.2 + 0.0j
bcx = [x.real]
# bcy = [1.0, li1(x).real, li2(x).real]
bcy = [0.0, li1(x).imag, li2(x).imag]


def ode_matrix(v):
    # diverges for x in {0,1}
    x = v[0]
    return [
        [0.0, 0.0, 0.0],
        [1.0 / (1.0 - x), 0.0, 0.0],
        [0.0, 1.0 / x, 0.0],
    ]
