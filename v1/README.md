# v1

Set configuration in:

* `system.py`
* `hyperparameters.py`
* `validation.py`

Run

```shell
pip install --user pipenv
pipenv install
pipenv shell
./train.py  # makes validation statistics and function plots
exit  # to exit pipenv shell
```

`plot.py` for deviation plot.

`train_standardised.py` introduces standardisation.
