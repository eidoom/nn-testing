#!/usr/bin/env python3

import torch, matplotlib.pyplot, matplotlib.ticker, numpy

from train import NeuralNetwork
import hyperparameters as h, validation as v

if __name__ == "__main__":
    print("Initialising")
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    x = torch.unsqueeze(
        torch.tensor(
            v.xx,
            device=device,
            dtype=h.dtype,
        ),
        dim=1,
    )

    model = NeuralNetwork(layers=h.layers).to(device)
    model.load_state_dict(torch.load("model.pth"))

    model.eval()
    with torch.no_grad():
        nn = model(x)
    nn = nn.cpu().numpy()

    rel_err = numpy.mean(numpy.abs((v.ff - nn) / v.ff), axis=0)
    print(f"mean |relative errors| = {rel_err}")

    plotname = "deviation.pdf"
    print(f"Plotting {plotname}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    weights = numpy.full(v.test_size, 1 / v.test_size)
    ax.xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
    )
    for i, (nni, ffi) in enumerate(zip(nn.T, v.ff.T)):
        ax.hist(
            numpy.log10(nni / ffi),
            bins=50,
            histtype="step",
            weights=weights,
            label=f"$f_{i}$",
            log=True,
        )
    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\log_{10}(NN/f)$")
    ax.legend()
    fig.savefig(plotname)
