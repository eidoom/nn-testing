#!/usr/bin/env python3

import math
import torch, matplotlib.pyplot, matplotlib.ticker, numpy

from train import NeuralNetwork, target

if __name__ == "__main__":
    print("Initialising")
    device = "cuda" if torch.cuda.is_available() else "cpu"
    model = NeuralNetwork(layers=[1] + [128] * 3 + [1]).to(device)
    model.load_state_dict(torch.load("model.pth"))

    n = int(1e4)

    print("Running NN")
    model.eval()
    with torch.no_grad():
        x = 2 * torch.rand(n, 1, device=device) - 1
        func = target(x)
        pred = model(x)

    x = x.cpu().numpy()
    func = func.cpu().numpy()
    pred = pred.cpu().numpy()

    diff = numpy.abs(pred - func)
    diff[diff == 0] = torch.finfo(torch.float32).resolution
    diff = numpy.log10(diff)

    rat = numpy.log10(pred / func)

    print("Plotting inference")
    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.scatter(x, func, label="Truth")
    ax.scatter(x, pred, label="Surrogate")
    ax.set_xlabel(r"$x$")
    ax.set_ylabel(r"$f(x)$")
    ax.legend()
    fig.savefig("inference.pdf")

    print("Plotting deviation")
    w = 9.6
    r = (1 + 5**0.5) / 2
    fig, axs = matplotlib.pyplot.subplots(
        nrows=2,
        ncols=2,
        sharex="col",
        tight_layout=True,
        figsize=(w, w / r),
        gridspec_kw={
            "hspace": 0,
        },
    )
    weights = numpy.full(n, 1 / n)

    bins = 50
    for ax in axs[:, 0]:
        ax.hist(diff, bins=bins, histtype="step", weights=weights)
        ax.set_ylabel("Proportion")

    axs[1, 0].set_xlabel(r"$\log_{10}(|NN-f|)$")
    axs[1, 0].set_yscale("log")

    pc = math.log10(1.01)
    # pm = math.log10(1.001)
    for ax in axs[:, 1]:
        ax.xaxis.set_major_locator(
            matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
        )
        ax.hist(rat, bins=bins, histtype="step", weights=weights)
        ax.axvline(pc, color="black")
        # ax.axvline(pm, color="black")

    axs[1, 1].set_xlabel(r"$\log_{10}(NN/f)$")
    axs[1, 1].set_yscale("log")

    axs[0, 1].text(x=pc, y=0, s=r"1%", ha="left", va="bottom")
    # axs[0, 1].text(x=pm, y=0, s=r"0.1%", ha="left", va="bottom")

    fig.savefig("cf.pdf")
