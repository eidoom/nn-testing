#!/usr/bin/env python3

import time
import torch, numpy, matplotlib.pyplot, matplotlib.ticker


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers):
        super().__init__()

        self.stack = torch.nn.Sequential(
            *[
                f(i)
                for i in range(len(layers) - 2)
                for f in (
                    lambda x: torch.nn.Linear(layers[x], layers[x + 1]),
                    lambda _: torch.nn.Tanh(),
                )
            ],
            torch.nn.Linear(layers[-2], layers[-1]),
        )

    def forward(self, x):
        return self.stack(x)


def target(x):
    return (
        89
        - 13 * x
        - 17 * x**2
        + 83 * x**3
        - 37 * x**4
        - 89 * x**5
        + 79 * x**6
        - 61 * x**7
    )


if __name__ == "__main__":
    print("Initialising")

    # hyperparameters
    learning_rate = 1e-2
    decay = 0.9
    batch_size = 100
    number_batches = 1000
    epochs = 20
    test_size = int(1e5)

    start = time.time()

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    torch.set_default_dtype(torch.float32)

    test_x = 2 * torch.rand(test_size, 1, device=device) - 1
    func = target(test_x)

    model = NeuralNetwork(layers=[1] + [128] * 3 + [1]).to(device)

    loss_fn = torch.nn.MSELoss()
    optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=decay)

    print("Training")
    print("epoch: validation metric")
    validation_metrics = []
    validation_losses = []
    lrs = []
    for t in range(1, 1 + epochs):
        # print(f"Epoch {t}/{epochs}\n-------------------------------")

        model.train()
        for batch in range(number_batches):
            # Use random points over domain x in [-1, 1]
            x = 2 * torch.rand(batch_size, 1, device=device) - 1

            # Compute prediction error
            pred = model(x)
            targ = target(x)
            loss = loss_fn(pred, targ)

            # Backpropagation
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()

            # print(f"loss: {loss.item():>6f}  [{batch:>3d}/{number_batches:>3d}]")

        model.eval()
        with torch.no_grad():
            pred = model(test_x)
            validation_loss = loss_fn(pred, func)
            validation_metric = torch.mean(torch.abs((pred - func) / func))

        # print(f"validation metric: {validation_metric:>6f}")
        validation_losses.append(validation_loss.cpu())
        validation_metrics.append(validation_metric.cpu())
        lrs.append(scheduler.get_last_lr())

        # reduce learning rate
        scheduler.step()

        print(f"{t:>5d}: {validation_metric:.6f}")

    validation_metrics = numpy.array(validation_metrics)
    validation_losses = numpy.array(validation_losses)
    lrs = numpy.array(lrs)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    print("Plotting validation")
    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(nbins="auto", integer=True)
    )
    ax.plot(validation_metrics, label="metric")
    ax.plot(1e-3 * validation_losses, label=r"loss $\times 10^{-3}$")
    ax.plot(1e1 * lrs, label=r"lr $\times 10^1$")
    ax.set_xlabel("Epoch")
    ax.legend()
    ax.set_yscale("linear")
    fig.savefig("validation-lin.pdf")
    ax.set_yscale("log")
    fig.savefig("validation-log.pdf")

    print("Saving model")
    torch.save(model.state_dict(), "model.pth")
