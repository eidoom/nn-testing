#!/usr/bin/env python3

import torch, numpy, matplotlib.pyplot

from func import NeuralNetwork, target

if __name__ == "__main__":
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    model = NeuralNetwork(layers=[1] + [20] * 3 + [1]).to(device)
    model.load_state_dict(torch.load("model.pth"))

    x = 2 * torch.rand(int(1e5), 1, device=device) - 1
    x, _ = torch.sort(x, 0)
    f = target(x)

    model.eval()
    with torch.no_grad():
        nn = model(x)

    devi = torch.flatten((nn - f) / f)

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.plot(x.cpu(), f.cpu(), label="Truth")
    ax.plot(x.cpu(), nn.cpu(), label="Surrogate")
    ax.set_xlabel("$x$")
    ax.set_ylabel("$f(x)$")
    ax.legend()
    fig.savefig("fit.pdf")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.hist(devi.cpu(), bins=50)
    ax.set_xlabel("|(NN-f)/f|")
    ax.set_ylabel("frequency")
    ax.set_yscale("linear")
    fig.savefig("deviation-lin.pdf")
    ax.set_yscale("log")
    fig.savefig("deviation-log.pdf")
