#!/usr/bin/env python3
"""
https://deepxde.readthedocs.io/en/latest/demos/function/func.html
"""

import deepxde, numpy


def func(x):
    return x * numpy.sin(5 * x)


if __name__ == "__main__":
    geom = deepxde.geometry.Interval(-1, 1)

    data = deepxde.data.Function(
        geom,
        func,
        num_train=16,
        num_test=100,
    )

    net = deepxde.nn.FNN(
        [1] + [20] * 3 + [1],
        activation="tanh",
        kernel_initializer="Glorot uniform",
    )

    model = deepxde.Model(data, net)
    model.compile(
        optimizer="adam",
        lr=1e-3,
        loss="MSE",
        metrics=["l2 relative error"],
    )
    loss_history, train_state = model.train(iterations=10000)

    deepxde.saveplot(loss_history, train_state, issave=True, isplot=True)
