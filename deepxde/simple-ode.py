#!/usr/bin/env python3
"""
https://deepxde.readthedocs.io/en/latest/demos/pinn_forward/ode.system.html
"""

import deepxde, numpy


def ode_system(x, y):
    """ODE system.
    dy1/dx = y2
    dy2/dx = -y1
    """
    y1, y2 = y[:, 0:1], y[:, 1:]
    dy1_x = deepxde.grad.jacobian(y, x, i=0)
    dy2_x = deepxde.grad.jacobian(y, x, i=1)

    return [dy1_x - y2, dy2_x + y1]


def boundary(_, on_initial):
    """
    selects t=0
    """
    return on_initial


def func(x):
    """
    y1 = sin(x)
    y2 = cos(x)
    """
    return numpy.hstack((numpy.sin(x), numpy.cos(x)))


if __name__ == "__main__":
    geom = deepxde.geometry.TimeDomain(0, 10)

    # y1(0) = 0
    ic1 = deepxde.icbc.IC(
        geom=geom,
        func=lambda x: 0,
        on_initial=boundary,
        component=0,
    )
    # y2(0) = 1
    ic2 = deepxde.icbc.IC(
        geom=geom,
        func=lambda x: 1,
        on_initial=boundary,
        component=1,
    )

    data = deepxde.data.PDE(
        geometry=geom,
        pde=ode_system,
        bcs=[ic1, ic2],
        num_domain=35,
        num_boundary=2,
        solution=func,
        num_test=100,
    )

    net = deepxde.nn.FNN(
        [1] + [50] * 3 + [2],
        activation="tanh",
        kernel_initializer="Glorot uniform",
    )

    model = deepxde.Model(data, net)
    model.compile(
        optimizer="adam",
        lr=1e-3,
        loss="MSE",
        metrics=["l2 relative error"],
    )
    loss_history, train_state = model.train(iterations=20000)

    deepxde.saveplot(loss_history, train_state, issave=True, isplot=True)
