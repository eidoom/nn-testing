#!/usr/bin/env python3
"""
https://deepxde.readthedocs.io/en/latest/demos/operator/antiderivative_aligned.html
Data: https://drive.google.com/drive/folders/1Sb0cLnMv8rCeQyBUk97VyKa5Le9BKQNb
"""

import deepxde, matplotlib.pyplot, numpy

if __name__ == "__main__":
    # Load dataset
    d = numpy.load("antiderivative_aligned_train.npz", allow_pickle=True)
    # the first element is the branch net input, the second element is the trunk net input
    # branch net input: the functions v
    # trunk net input: the locations x of u(x) values
    X_train = (d["X"][0].astype(numpy.float32), d["X"][1].astype(numpy.float32))
    y_train = d["y"].astype(numpy.float32)
    d = numpy.load("antiderivative_aligned_test.npz", allow_pickle=True)
    X_test = (d["X"][0].astype(numpy.float32), d["X"][1].astype(numpy.float32))
    y_test = d["y"].astype(numpy.float32)

    # https://deepxde.readthedocs.io/en/latest/modules/deepxde.data.html#deepxde.data.triple.TripleCartesianProd
    data = deepxde.data.TripleCartesianProd(
        X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test
    )

    # Choose a network
    m = 100
    dim_x = 1
    # https://deepxde.readthedocs.io/en/latest/modules/deepxde.nn.pytorch.html?highlight=DeepONet#deepxde.nn.pytorch.deeponet.DeepONetCartesianProd
    net = deepxde.nn.DeepONetCartesianProd(
        layer_sizes_branch=[m, 40, 40],
        layer_sizes_trunk=[dim_x, 40, 40],
        activation="relu",
        kernel_initializer="Glorot normal",
    )

    # Define a Model
    # https://deepxde.readthedocs.io/en/latest/modules/deepxde.html#deepxde.model.Model
    model = deepxde.Model(data, net)

    # Compile and Train
    model.compile(
        optimizer="adam",
        lr=1e-3,
        loss="MSE",  # https://deepxde.readthedocs.io/en/latest/_modules/deepxde/losses.html
        metrics=[
            "mean l2 relative error"
        ],  # https://deepxde.readthedocs.io/en/latest/modules/deepxde.html?highlight=metrics#module-deepxde.metrics
    )
    loss_history, train_state = model.train(iterations=10000)

    # Plot the loss trajectory
    deepxde.utils.plot_loss_history(loss_history)
    matplotlib.pyplot.show()
