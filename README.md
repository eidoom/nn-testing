# [nn-testing](https://gitlab.com/eidoom/nn-testing)

See also [Feynman Integrals NN](https://gitlab.com/eidoom/feynman-integrals-nn)

## Description of this repo

### Testing

* function
    * `func_approx`
* ode
    * `ode`: scalar -> scalar with polynomial functions
    * `ode-nonlinear`: scalar -> scalar with non-linear functions
    * `ode-vec`: scalar -> vector with non-linear functions
* symbolic ode
    * `ode-trig`: trigonometric functions
    * `pl-ode`: polylogarithms (PLs)

### Demonstration

* `v1`: weight-2 (w2) PLs, real-valued functions
* `v1_complex`: w2 PLs, try using torch built-in complex support before realising inappropriate (fail)
* `v1_complex_big`: w3 PLs, as `v1_complex` (fail)
* `v1_complex_alt`: w2 PLs, handle complex numbers manually (successful)
* `v1_eps`: w2 PLs, handle epsilon expansion
* `v1_eps_big`: w4 PLs, as `v1_eps`
* `v1_box`: 4-point 1-loop Feynman integral system (O(eps^3): w3 PLs), handle multiple input variables

## ML frameworks

### PyTorch

* https://pytorch.org/
* https://en.wikipedia.org/wiki/PyTorch
* https://github.com/pytorch/pytorch
* https://pypi.org/project/torch/
* https://arxiv.org/abs/1912.01703

### TensorFlow

* https://www.tensorflow.org/
* https://en.wikipedia.org/wiki/TensorFlow
* https://github.com/tensorflow/tensorflow
* https://pypi.org/project/tensorflow/
* https://arxiv.org/abs/1603.04467

#### Python API: Keras

* https://keras.io/
* https://en.wikipedia.org/wiki/Keras
* https://github.com/keras-team/keras
* https://pypi.org/project/keras/

### scikit-learn

* https://scikit-learn.org/stable/
* https://en.wikipedia.org/wiki/Scikit-learn
* https://github.com/scikit-learn/scikit-learn
* https://arxiv.org/abs/1201.0490, https://arxiv.org/abs/1309.0238

## PDE-NN

### Libraries

Overview of available libraries and comparison to traditional methods
* https://arxiv.org/abs/2201.03269

#### DeepXDE

* https://deepxde.readthedocs.io/en/latest/
* https://github.com/lululxvi/deepxde
* https://pypi.org/project/DeepXDE/
* https://arxiv.org/abs/1907.04502

#### IDRLnet

* https://idrlnet.readthedocs.io/en/latest/
* https://github.com/idrl-lab/idrlnet
* https://pypi.org/project/idrlnet/
* https://arxiv.org/abs/2107.04320

#### NeuralPDE.jl

* https://docs.sciml.ai/NeuralPDE/stable/
* https://github.com/SciML/NeuralPDE.jl
* https://arxiv.org/abs/2107.09443

#### DyDEns

* https://github.com/analysiscenter/pydens
* https://pypi.org/project/pydens/
* https://arxiv.org/abs/1909.11544
* https://medium.com/data-analysis-center/solving-differential-equations-using-neural-networks-with-pydens-e831f1a115f

#### NeuralOperators.jl

* https://docs.sciml.ai/NeuralOperators/stable/
* https://github.com/SciML/NeuralOperators.jl

#### NeuroDiffEq

* https://neurodiffeq.readthedocs.io/en/latest/
* https://github.com/NeuroDiffGym/neurodiffeq
* https://pypi.org/project/neurodiffeq/
* https://joss.theoj.org/papers/10.21105/joss.01931

### Code examples

* https://github.com/aditya-jaishankar/pde-nn
* https://github.com/zxx2643/nn-pde-solver
* https://github.com/TariqBerrada/Physics-informed-neural-networks
* https://jparkhill.netlify.app/solvingdiffusions/
* https://github.com/juansensio/nangs
* https://github.com/pooyasf/DGM

### Literature

#### Textbooks

* https://www.deeplearningbook.org/

#### Reviews

* [An overview on deep learning-based approximation methods for partial differential equations](https://arxiv.org/abs/2012.12348)
* [Partial Differential Equations Meet Deep Neural Networks: A Survey](https://arxiv.org/abs/2211.05567)
* [Three Ways to Solve Partial Differential Equations with Neural Networks -- A Review](https://arxiv.org/abs/2102.11802)

#### Neural operators

* https://www.quantamagazine.org/latest-neural-nets-solve-worlds-hardest-equations-faster-than-ever-before-20210419/
* [Neural Operator: Learning Maps Between Function Spaces](https://arxiv.org/abs/2108.08481)
    * https://github.com/neural-operator
* [DeepONet: Learning nonlinear operators for identifying differential equations based on the universal approximation theorem of operators](https://arxiv.org/abs/1910.03193)
* [Fourier Neural Operator for Parametric Partial Differential Equations](https://arxiv.org/abs/2010.08895) (FNO)
* [KoopmanLab: A PyTorch module of Koopman neural operator family for solving partial differential equations](https://arxiv.org/abs/2301.01104) (KNO)

#### PINN

* Physics-Informed Neural Network
* https://github.com/maziarraissi/PINNs
* evolved from DGM?
    * [DGM: A deep learning algorithm for solving partial differential equations](https://arxiv.org/abs/1708.07469)
* [State-of-the-Art Review of Design of Experiments for Physics-Informed Deep Learning](https://arxiv.org/abs/2202.06416)

#### NeRF

* Neural Radiance Field
* [Fast Neural Network based Solving of Partial Differential Equations](https://arxiv.org/abs/2205.08978)

#### Historic

* [Artificial Neural Networks for Solving Ordinary and Partial Differential Equations](https://arxiv.org/abs/physics/9705023)

#### HEP

* [Elvet -- a neural network-based differential equation and variational problem solver](https://arxiv.org/abs/2103.14575)
* [Multi-variable Integration with a Neural Network](https://arxiv.org/abs/2211.02834)
* [Targeting Multi-Loop Integrals with Neural Networks](https://arxiv.org/abs/2112.09145)

#### Other articles

* [Solving Partial Differential Equations with Neural Networks](https://arxiv.org/abs/1912.04737)
* [dNNsolve: an efficient NN-based PDE solver](https://arxiv.org/abs/2103.08662v1)
* [Solving parametric PDE problems with artificial neural networks](https://arxiv.org/abs/1707.03351)
* [Multi-Output Physics-Informed Neural Networks for Forward and Inverse PDE Problems with Uncertainties](https://arxiv.org/abs/2202.01710)
* [SPINN: Sparse, Physics-based, and partially Interpretable Neural Networks for PDEs](https://arxiv.org/abs/2102.13037)
* [A Discussion on Solving Partial Differential Equations using Neural Networks](https://arxiv.org/abs/1904.07200)
* [Physics-Informed Neural Operator for Learning Partial Differential Equations](https://arxiv.org/abs/2111.03794)
* [Fourier Neural Operator with Learned Deformations for PDEs on General Geometries](https://arxiv.org/abs/2207.05209)

## Jargon

* Forward problem: cause -> effect
* Inverse problem: effect -> cause

### Differential equations

* https://en.wikipedia.org/wiki/Differential_equation
* ordinary (ODE)
    * https://en.wikipedia.org/wiki/Ordinary_differential_equation
    * univariate
* partial (PDF)
    * https://en.wikipedia.org/wiki/Partial_differential_equation
    * multivariate
* linear (LDE)
    * https://en.wikipedia.org/wiki/Linear_differential_equation
    * equation is linear polynomial in function and its derivatives
* homogeneous
    * https://en.wikipedia.org/wiki/Homogeneous_differential_equation
    * f(x,y) dy = g(x,y) dx
* order
    * order of highest derivative
    * eg. 2nd order if $d^2f/dx^2$ exists
* stochastic (SDE)
    * https://en.wikipedia.org/wiki/Stochastic_differential_equation
    * includes random term

### Boundary conditions

* Dirichlet
    * https://en.wikipedia.org/wiki/Dirichlet_boundary_condition
    * condition on boundary value
* Neumann
    * https://en.wikipedia.org/wiki/Neumann_boundary_condition
    * condition on boundary derivative
* Robin
    * https://en.wikipedia.org/wiki/Robin_boundary_condition
    * condition on linear combination of function values and derivatives on boundary
