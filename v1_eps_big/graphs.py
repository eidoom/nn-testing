import math
import matplotlib.pyplot, matplotlib.cm, numpy

import system as s


def plot_validation_inference(xx, ff, nn):
    inf_plot_name = "inference.pdf"
    print(f"Plotting {inf_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    # ax.set_yscale("log")

    ax.set_xlabel("$x$")
    ax.set_ylabel("$f(x)$")

    colours = numpy.concatenate(
        (
            matplotlib.cm.tab20(range(20)),
            matplotlib.cm.tab20b(range(0, 20, 2)),
            matplotlib.cm.tab20c(range(0, 20, 2)),
        )
    )
    c = 0

    for i, z in enumerate(zip(ff.T, nn.T)):
        for j, (fij, nij) in enumerate(zip(*z)):
            na = nij.real
            nb = nij.imag

            ta = not numpy.isclose(na, 0.0, atol=1e-1).all()
            tb = not numpy.isclose(nb, 0.0, atol=1e-1).all()

            if ta:
                ax.plot(
                    xx,
                    fij.real,
                    label=f"$\mathrm{{Re}}\,f_{i}^{{({j})}}$",
                    linestyle="solid",
                    color=colours[2 * c],
                )
                ax.plot(
                    xx,
                    na,
                    linestyle="dashed",
                    color=colours[2 * c],
                )

            if tb:
                ax.plot(
                    xx,
                    fij.imag,
                    label=f"$\mathrm{{Im}}\,f_{i}^{{({j})}}$",
                    linestyle="solid",
                    color=colours[2 * c + 1],
                )
                ax.plot(
                    xx,
                    nb,
                    linestyle="dashed",
                    color=colours[2 * c + 1],
                )

            if ta or tb:
                c += 1
                c %= 20

    # ax.legend(bbox_to_anchor=(1, 0.5), loc="center left")

    fig.savefig(inf_plot_name, bbox_inches="tight")


def plot_validation_statistics(lrs, validation_metrics, validation_losses):
    val_plot_name = "validation-log.pdf"
    print(f"Plotting {val_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")
    ax.set_yscale("log")

    ax.plot(lrs, label="Learning rate")
    ax.plot(validation_metrics, label="Validation metric")
    ax.plot(validation_losses, label="Validation loss")

    ax.legend()

    fig.savefig(val_plot_name, bbox_inches="tight")


def plot_validation_deviation(ff, nn):
    plotname = "deviation.pdf"
    print(f"Plotting {plotname}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
    )

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\log_{10}(NN/f)$")

    n = ff.shape[0]

    weights = numpy.full(n, 1 / n)

    mff = numpy.ma.masked_equal(ff, 0)

    r = numpy.log10(numpy.absolute(nn / mff))

    r = r.swapaxes(0, 2)

    b = numpy.histogram_bin_edges(r.compressed(), bins="auto")

    for i, ri in enumerate(r):
        for j, rij in enumerate(ri):
            if rij.compressed().size:
                ax.hist(
                    rij,
                    bins=b,
                    histtype="step",
                    weights=weights,
                    label=f"$f_{i}^{{({j})}}$",
                )

    ax.axvline(math.log10(1.01), color="black", linewidth=1)
    ax.axvline(math.log10(0.99), color="black", linewidth=1)

    ax.text(
        0.5,
        0.9,
        "$1\%$",
        horizontalalignment="center",
        verticalalignment="center",
        transform=ax.transAxes,
    )

    ax.legend()

    fig.savefig(plotname, bbox_inches="tight")
