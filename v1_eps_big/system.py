import math, random
import mpmath


def f(v):
    # i^th row for coefficient of eps^i: [eps^0, eps^1, eps^2, eps^3]
    # j^th column for function j: [f_0, f_1, f_2, f_3, f_4, f_5, f_6]
    x = v[0]
    return [
        [
            1,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        [
            0,
            mpmath.polylog(1, x),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        [
            0,
            0,
            mpmath.polylog(1, x) ** 2,
            mpmath.polylog(2, x),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ],
        [
            0,
            0,
            0,
            0,
            mpmath.polylog(1, x) ** 3,
            mpmath.polylog(1, x) * mpmath.polylog(2, x),
            mpmath.polylog(3, x),
            0,
            0,
            0,
            0,
            0,
        ],
        [
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            mpmath.polylog(1, x) ** 4,
            mpmath.polylog(1, x) ** 2 * mpmath.polylog(2, x),
            mpmath.polylog(2, x) ** 2,
            mpmath.polylog(1, x) * mpmath.polylog(3, x),
            mpmath.polylog(4, x),
        ],
    ]


num_in = 1
num_eps = 5
num_func = 12

cut = 0.1
poles = [0.0, 1.0]  # poles in f and df

# analytically continued region, solution is complex
domain_l = 1.0
domain_r = domain_l + math.pi

bcx = [[domain_l + 2 * cut], [domain_r - 2 * cut]]
bcy = [f(x) for x in bcx]


def ode_matrix(v):
    # diverges for x in {0,1}
    # assume form A^0 + eps A^1
    # real valued
    x = v[0]
    return [
        [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [1 / (1 - x), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 2 / (1 - x), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1 / x, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 3 / (1 - x), 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 1 / x, 1 / (1 - x), 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1 / x, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 4 / (1 - x), 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 2 / (1 - x), 1 / x, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 2 / x, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 1 / x, 1 / (1 - x), 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 1 / x, 0, 0, 0, 0, 0],
        ],
    ]


assert len(bcx) == len(bcy)
assert len(bcx[0]) == num_in
assert len(bcy[0]) == num_eps
assert len(bcy[0][0]) == num_func
p = random.random()
m = ode_matrix([p])
assert len(m) == 2
assert len(m[0]) == num_func
assert len(m[0][0]) == num_func
