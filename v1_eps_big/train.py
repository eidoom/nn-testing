#!/usr/bin/env python3

import time, random
import torch, numpy, matplotlib.pyplot, matplotlib.cm

import system as s, hyperparameters as h, validation as v, phasespace as p, graphs as g


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers, dtype_real):
        super().__init__()

        self.stacked = torch.nn.Sequential(
            *[
                f(x)
                for x in zip(layers[:-2], layers[1:-1])
                for f in (
                    lambda x: torch.nn.Linear(*x, dtype=dtype_real),
                    lambda _: torch.nn.Tanh(),
                )
            ],
            torch.nn.Linear(layers[-2], layers[-1], dtype=dtype_real),
        )

        for i in range(0, len(self.stacked), 2):
            torch.nn.init.xavier_uniform_(
                self.stacked[i].weight, gain=torch.nn.init.calculate_gain("tanh")
            )

    def forward(self, x):
        # order (f_i = sum_j f_i^(j) e^j)
        # {Re[f_0^(0)], Re[f_1^(0)], Im[f_0^(0)], Im[f_1^(0)], Re[f_0^(1)], Re[f_1^(1)], Im[f_0^(1)], Im[f_1^(1)]}
        return self.stacked(x)


class MyLoss(torch.nn.Module):
    def __init__(self, x, y, model, device, dtype_real, dtype_complex):
        super().__init__()
        self.bcx = torch.tensor(x, device=device, dtype=dtype_real)
        self.bcy = torch.stack(
            [
                torch.view_as_real(torch.tensor(yy, device=device, dtype=dtype_complex))
                .transpose(1, 2)
                .flatten()
                for yy in y
            ]
        )
        self.model = model
        self.device = device
        self.dtype_real = dtype_real

    def forward(self, x, mats, pred, training=True):
        # input_dim = 1
        batch_dim = x.size(dim=0)
        output_dim = pred.size(dim=1)

        loss = torch.nn.MSELoss()

        # loop over outputs of NN
        dnn = (
            torch.stack(
                [
                    torch.autograd.grad(
                        outputs=pred[:, i],
                        inputs=x,
                        grad_outputs=torch.ones(
                            batch_dim, device=self.device, dtype=self.dtype_real
                        ),
                        retain_graph=training or i != output_dim - 1,
                        create_graph=training,
                    )[0]
                    for i in range(output_dim)
                ]
            )
            .view(s.num_eps, 2 * s.num_func, batch_dim)
            .transpose(1, 2)
        )
        # print("dnn", dnn.size())

        # stack up as interleaved Re/Im per eps order
        pred = (
            pred.view(batch_dim, s.num_eps, 2 * s.num_func)
            .transpose(0, 1)
            .reshape(s.num_eps, 2 * batch_dim, s.num_func, 1)
        )
        # print("pred", pred.size())

        # these broadcast over batch+(re/im) dimension
        target = torch.empty(
            (s.num_eps, 2 * batch_dim, s.num_func, 1),
            device=self.device,
            dtype=self.dtype_real,
        )
        target[0] = torch.matmul(mats[0], pred[0])

        for k in range(1, s.num_eps):
            target[k] = torch.matmul(mats[0], pred[k]) + torch.matmul(
                mats[1], pred[k - 1]
            )

        target = target.view(s.num_eps, batch_dim, 2 * s.num_func)
        # print("target", target.size())

        bulk = loss(dnn, target)

        bv = self.model(self.bcx)
        # print("bv", bv.size())
        # print("bcy", self.bcy.size())
        boundary = loss(bv, self.bcy)
        # exit()

        return bulk + boundary


def as_complex(x, ne, nf):
    return torch.view_as_complex(x.reshape(-1, ne, 2, nf).transpose(2, 3).contiguous())


def mats_num(df, x, device, dtype):
    return (
        torch.stack([torch.tensor(df(xx), device=device, dtype=dtype) for xx in x])
        .repeat_interleave(repeats=2, dim=0)
        .transpose(0, 1)
    )


def train(model, x_train, loss_fn, mats_train, optimiser):
    # Compute prediction error
    pred_train = model(x_train)
    loss_train = loss_fn(x_train, mats_train, pred_train)

    # Backpropagation
    optimiser.zero_grad()
    loss_train.backward()
    optimiser.step()

    return loss_train


if __name__ == "__main__":
    print("Initialising")
    start = time.time()

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"Device: {device}")

    model = NeuralNetwork(layers=h.layers, dtype_real=h.dtype_real).to(device)

    loss_fn = MyLoss(
        x=s.bcx,
        y=s.bcy,
        model=model,
        device=device,
        dtype_real=h.dtype_real,
        dtype_complex=h.dtype_complex,
    )

    optimiser = torch.optim.Adam(
        model.parameters(),
        lr=h.learning_rate,
        betas=(0.9, 0.999),
        eps=1e-8,
        weight_decay=h.l2_regularisation_weight,
        fused=device == torch.device("cuda"),
    )

    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=h.decay)

    metric = torch.nn.L1Loss()

    # grad required for validation_metrics loss
    x = torch.tensor(
        v.xx,
        device=device,
        requires_grad=True,
        dtype=h.dtype_real,
    )

    mats = mats_num(s.ode_matrix, x, device=device, dtype=h.dtype_real)

    f = torch.tensor(v.ff, device=device, dtype=h.dtype_complex)

    regions = p.get_regions(h.batch_size)
    dists = [torch.distributions.uniform.Uniform(r["start"], r["end"]) for r in regions]
    x_train = (
        torch.concatenate(
            [dist.sample((r["number"], s.num_in)) for dist, r in zip(dists, regions)]
        )
        .to(device)
        .to(h.dtype_real)
        .requires_grad_()
    )

    mats_train = mats_num(s.ode_matrix, x_train, device=device, dtype=h.dtype_real)

    dur = time.time() - start
    print(f"Initialisation took {dur:.1f}s")

    print("Training")
    start = time.time()

    lrs = []
    validation_metrics = []
    validation_losses = []
    for t in range(h.epochs):
        epoch_start = time.time()

        lrs += scheduler.get_last_lr()
        print(
            f"Epoch {t+1}/{h.epochs} (lr: {lrs[-1]:.4f})\n-------------------------------"
        )

        model.train()
        for batch in range(h.number_batches):
            loss_train = train(model, x_train, loss_fn, mats_train, optimiser)

            if not batch % (h.number_batches // 10):
                print(
                    f"loss: {loss_train:9.6f}  [{h.batch_size*batch:>7d}/{h.batch_size*h.number_batches:>8d}]"
                )

        model.eval()
        pred_valid = model(x)
        loss_valid = loss_fn(x, mats, pred_valid, training=False).detach().cpu()
        metric_valid = metric(
            as_complex(pred_valid.detach(), s.num_eps, s.num_func), f
        ).cpu()

        print(f"validation metric: {metric_valid:.6f}")
        print(f"             loss: {loss_valid:.6f}")
        print(f"       epoch took: {time.time() - epoch_start:.1f}s")

        validation_metrics.append(metric_valid)
        validation_losses.append(loss_valid)
        scheduler.step()

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    model_name = "model.pth"
    print(f"Saving model {model_name}")
    torch.save(model.state_dict(), model_name)

    g.plot_validation_statistics(lrs, validation_metrics, validation_losses)

    nn = as_complex(pred_valid.detach(), s.num_eps, s.num_func).cpu().numpy()
    g.plot_validation_inference(v.xx, v.ff, nn)
