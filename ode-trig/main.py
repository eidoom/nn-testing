#!/usr/bin/env python3

import time, math
import torch, numpy, matplotlib.pyplot


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers):
        super().__init__()

        self.input_dim = layers[0]
        self.output_dim = layers[-1]

        tmp = []
        for a, b in zip(layers[:-2], layers[1:-1]):
            tmp += [torch.nn.Linear(a, b), torch.nn.Tanh()]

        self.stack = torch.nn.Sequential(*tmp, torch.nn.Linear(layers[-2], layers[-1]))

    def forward(self, x):
        return self.stack(x)


class MyLoss(torch.nn.Module):
    def __init__(self, df, x, y, model, device):
        super().__init__()
        self.df = lambda v: torch.tensor(df(v), device=device, dtype=torch.float32)
        self.bcx = torch.tensor(x, device=device, dtype=torch.float32)
        self.bcy = torch.tensor(y, device=device, dtype=torch.float32)
        self.model = model
        self.device = device

    def forward(self, x, pred, training=True):
        batch_dim = x.size(dim=0)
        output_dim = pred.size(dim=1)

        loss = torch.nn.MSELoss()

        # loop over outputs of NN
        dnn = torch.stack(
            [
                torch.autograd.grad(
                    outputs=pred[:, i],
                    inputs=x,
                    grad_outputs=torch.ones(batch_dim, device=self.device),
                    # retain_graph=training or i != output_dim - 1,
                    retain_graph=True,
                    create_graph=training,
                )[0]
                for i in range(output_dim)
            ]
        ).transpose(0, 1)

        mats = torch.stack([self.df(xx) for xx in x])

        # transpose pred from row [dim=(batch dim, nn output dim)] to vector [dim=(batch dim, nn output dim, 1)]
        pred = torch.unsqueeze(pred, dim=2)

        # this broadcasts over batch dimension
        target = torch.matmul(mats, pred)

        bulk = loss(dnn, target)

        bv = self.model(self.bcx)
        boundary = loss(bv, self.bcy)

        # consider regularisation, weighting boundary
        return bulk + boundary


def ode_matrix(v):
    x = v[0]
    return [
        [0.0, 1.0],
        [-1.0, 0.0],
    ]


if __name__ == "__main__":
    print("Initialising")

    # hyperparameters
    learning_rate = 1e-2
    decay = 0.9
    batch_size = 100
    number_batches = 100
    epochs = 10
    test_size = 1000

    domain_l = 0
    domain_r = 2.0 * math.pi

    torch.set_default_dtype(torch.float32)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    model = NeuralNetwork(layers=[1] + [128] * 3 + [2]).to(device)

    loss_fn = MyLoss(
        df=ode_matrix,
        x=[0.0],
        y=[0.0, 1.0],
        model=model,
        device=device,
    )

    optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=decay)

    print("Preparing validation data")
    xx = numpy.linspace(
        start=domain_l,
        stop=domain_r,
        num=test_size,
        endpoint=True,
    )

    x = torch.reshape(
        torch.tensor(
            xx,
            device=device,
            requires_grad=True,
            dtype=torch.float32,
        ),
        (test_size, 1),
    )  # grad required for validation_metrics loss

    ff = numpy.stack((numpy.sin(xx), numpy.cos(xx)), axis=1)
    f = torch.tensor(ff, device=device)

    print("Training")
    print(f"Batch size: {batch_size}")
    start = time.time()

    dist = torch.distributions.uniform.Uniform(domain_l, domain_r)

    lrs = []
    validation_metrics = []
    validation_losses = []
    for t in range(1, 1 + epochs):
        lrs.append(scheduler.get_last_lr())
        print(
            f"Epoch {t}/{epochs} (lr: {lrs[-1][0]:.4f})\n-------------------------------"
        )

        model.train()
        for batch in range(number_batches):
            # Compute prediction error
            x_train = dist.sample((batch_size, 1)).to(device).requires_grad_()
            pred = model(x_train)
            loss = loss_fn(x_train, pred)

            # Backpropagation
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()

            if not batch % (number_batches // 10):
                print(f"loss: {loss:9.6f}  [{batch:>3d}/{number_batches:>4d}]")

        model.eval()
        pred = model(x)
        validation_loss = loss_fn(x, pred, training=False).detach()
        validation_metric = torch.mean(torch.abs((pred.detach() - f) / f))
        print(f"validation metric: {validation_metric:.6f}")
        print(f"             loss: {validation_loss:.6f}")

        validation_metrics.append(validation_metric.cpu())
        validation_losses.append(validation_loss.cpu())
        scheduler.step()

    lrs = numpy.array(lrs)
    validation_metrics = numpy.array(validation_metrics)
    validation_losses = numpy.array(validation_losses)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    print("Running NN")
    model.eval()
    x = x.detach()
    with torch.no_grad():
        nn = model(x)

    nn = nn.cpu().numpy()

    print("Plotting")

    rat = numpy.log10(nn / ff)

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.plot(lrs, label="Learning rate")
    ax.plot(validation_metrics, label="Validation metric")
    ax.plot(validation_losses, label="Validation loss")
    ax.legend()
    ax.set_yscale("log")
    fig.savefig("validation-log.pdf")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    for i, fff in enumerate(ff.T):
        ax.scatter(xx, fff, label=f"f{i} truth")
    for i, nnn in enumerate(nn.T):
        ax.scatter(xx, nnn, label=f"f{i} surrogate")
    ax.legend()
    fig.savefig("inference.pdf")

    # print("Saving model")
    # torch.save(model.state_dict(), "model.pth")

    print("Done!")
