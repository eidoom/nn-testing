#!/usr/bin/env python3

import math

import torch, numpy, matplotlib.pyplot

from ode import (
    NeuralNetwork,
    inference,
    init_torch,
    training,
    plot_validation,
    plot_inference,
)

from nonlinear import (
    li1,
    li2,
)


class MyLoss(torch.nn.Module):
    def __init__(self, df, x, y, model, device):
        super().__init__()
        self.df = df
        self.x = torch.tensor(x, device=device, dtype=torch.float32)
        self.y = torch.tensor(y, device=device, dtype=torch.float32)
        self.model = model
        self.device = device

    def forward(self, x, pred, training=True):
        batch_dim = x.size(dim=0)
        output_dim = pred.size(dim=1)

        loss = torch.nn.MSELoss()

        dnn = torch.stack(
            [
                torch.autograd.grad(
                    outputs=pred[:, i],
                    inputs=x,
                    grad_outputs=torch.ones(batch_dim, device=self.device),
                    retain_graph=True,
                    create_graph=training,
                )[0]
                for i in range(output_dim)
            ]
        ).transpose(0, 1)

        target = self.df(x)
        bulk = loss(dnn, target)

        bv = self.model(self.x)
        boundary = loss(bv, self.y)

        return bulk + boundary


if __name__ == "__main__":
    print("Initialising")

    # hyperparameters
    learning_rate = 1e-2
    decay = 0.9
    batch_size = 100
    number_batches = 100
    epochs = 5
    test_size = 1000

    domain_l = -1.0 - 2.0 * math.pi
    domain_r = -1.0

    device = init_torch()

    model = NeuralNetwork(layers=[1] + [128] * 3 + [2]).to(device)

    ff = lambda z: numpy.transpose([li2(z) + li1(z), -1.0 / z])

    loss_fn = MyLoss(
        df=lambda z: torch.stack(
            [-torch.log(1.0 - z) / z + 1.0 / (1.0 - z), 1.0 / torch.pow(z, 2)], dim=1
        ),
        x=[-1.0],
        y=ff(-1.0),
        model=model,
        device=device,
    )

    x_ = numpy.linspace(
        start=domain_l,
        stop=domain_r,
        num=test_size,
        endpoint=False,
    )

    x = torch.reshape(
        torch.tensor(
            x_,
            device=device,
            requires_grad=True,
            dtype=torch.float32,
        ),
        (test_size, 1),
    )  # grad required for validation_metrics loss

    f_ = ff(x_)
    f = torch.tensor(f_, device=device, dtype=torch.float32)

    lrs, validation_metrics, validation_losses = training(
        loss_fn,
        model,
        device,
        learning_rate,
        decay,
        domain_l,
        domain_r,
        batch_size,
        number_batches,
        epochs,
        x,
        f,
    )

    x = x.detach()
    nn = inference(model, x)

    nn = nn.cpu().numpy()

    print("Plotting")

    plot_validation(
        lrs, validation_metrics, validation_losses, name="validation-vec.pdf"
    )

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.scatter(x_, f_[:, 0], label="Truth 1")
    ax.scatter(x_, nn[:, 0], label="Surrogate 1")
    ax.scatter(x_, f_[:, 1], label="Truth 2")
    ax.scatter(x_, nn[:, 1], label="Surrogate 2")
    ax.legend()
    fig.savefig("inference-vec.pdf")

    # plot_deviation(f_, nn, name="deviation-vec.pdf")

    print("Saving model")
    torch.save(model.state_dict(), "model-vec.pth")

    print("Done!")
