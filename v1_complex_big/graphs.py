import matplotlib.pyplot, matplotlib.cm


def plot_validation_inference(xx, ff, nn):
    inf_plot_name = "inference.pdf"
    print(f"Plotting {inf_plot_name}")

    n = len(ff[0])

    colours = matplotlib.cm.tab20(range(2 * n))

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)

    ax.set_xlabel("$x$")
    ax.set_ylabel("$f(x)$")

    for i, fff in enumerate(ff.T):
        ax.plot(
            xx,
            fff.real,
            # label=f"Re[$f_{i}$] truth",
            label=f"Re[$f_{i}$]",
            linestyle="solid",
            color=colours[2 * i],
        )

        ax.plot(
            xx,
            fff.imag,
            # label=f"Im[$f_{i}$] truth",
            label=f"Im[$f_{i}$]",
            linestyle="solid",
            color=colours[2 * i + 1],
        )

    for i, nnn in enumerate(nn.T):
        ax.plot(
            xx,
            nnn.real,
            # label=f"Re[$f_{i}$] NN",
            linestyle="dashed",
            color=colours[2 * i],
        )

        ax.plot(
            xx,
            nnn.imag,
            # label=f"Im[$f_{i}$] NN",
            linestyle="dashed",
            color=colours[2 * i + 1],
        )

    ax.legend(bbox_to_anchor=(1, 0.5), loc="center left")

    fig.savefig(inf_plot_name, bbox_inches="tight")
