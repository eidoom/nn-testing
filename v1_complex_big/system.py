import math
import mpmath


def f(x):
    return [
        1,
        mpmath.polylog(1, x),
        mpmath.polylog(1, x) ** 2,
        mpmath.polylog(2, x),
        mpmath.polylog(1, x) ** 3,
        mpmath.polylog(1, x) * mpmath.polylog(2, x),
        mpmath.polylog(3, x),
    ]


cut = 0.1
poles = [0.0, 1.0]  # poles in f and df

# analytically continued region, solution is complex
domain_l = 1.0
domain_r = domain_l + math.pi


x = 1.2
bcx = [x]
bcy = f(x)


def ode_matrix(v):
    # diverges for x in {0,1}
    x = v[0]
    return [
        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [1.0 / (1.0 - x), 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 2.0 / (1.0 - x), 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 1.0 / x, 0.0, 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 3.0 / (1.0 - x), 0.0, 0.0, 0.0, 0.0],
        [0.0, 0.0, 1.0 / x, 1.0 / (1.0 - x), 0.0, 0.0, 0.0],
        [0.0, 0.0, 0.0, 1.0 / x, 0.0, 0.0, 0.0],
    ]
