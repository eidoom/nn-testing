import numpy, mpmath

from system import f
from phasespace import get_regions

test_size = 1000

xx = numpy.concatenate(
    [
        numpy.linspace(
            start=region["start"],
            stop=region["end"],
            num=region["number"],
        )
        for region in get_regions(test_size)
    ]
)

ff = numpy.array([f(x) for x in xx], dtype=numpy.cdouble)
