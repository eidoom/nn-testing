import numpy
import system as s
from phasespace import get_regions

test_size = 1000

xx = numpy.concatenate(
    [
        numpy.linspace(
            start=region["start"],
            stop=region["end"],
            num=region["number"],
            dtype=numpy.cdouble,
        )
        for region in get_regions(test_size)
    ]
)

ff = numpy.stack(
    # (numpy.ones_like(xx), s.li1(xx), s.li2(xx)),
    (numpy.ones_like(xx), s.li1(xx), s.li2(xx), s.li1(xx) ** 2, s.li1(xx) * s.li2(xx)),
    axis=1,
)

xx = xx.real
