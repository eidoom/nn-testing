#!/usr/bin/env python3

import time, random
import torch, numpy, matplotlib.pyplot, matplotlib.cm

import system as s, hyperparameters as h, validation as v, phasespace as p, graphs as g


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers, dtype_real):
        super().__init__()

        output_dim = 2 * layers[-1]

        self.stack = torch.nn.Sequential(
            *[
                f(x)
                for x in zip(layers[:-2], layers[1:-1])
                for f in (
                    lambda x: torch.nn.Linear(*x, dtype=dtype_real),
                    lambda _: torch.nn.Tanh(),
                )
            ],
            torch.nn.Linear(layers[-2], output_dim, dtype=dtype_real),
        )

        for i in range(0, len(self.stack), 2):
            torch.nn.init.xavier_uniform_(
                self.stack[i].weight, gain=torch.nn.init.calculate_gain("tanh")
            )

    def forward(self, x):
        return self.stack(x)


class MyLoss(torch.nn.Module):
    def __init__(self, x, y, model, device, dtype_real, dtype_complex):
        super().__init__()
        self.bcx = torch.tensor(x, device=device, dtype=dtype_real)
        self.bcy = (
            torch.view_as_real(torch.tensor(y, device=device, dtype=dtype_complex))
            .transpose(0, 1)
            .flatten()
        )
        self.model = model
        self.device = device
        self.dtype_real = dtype_real

    def forward(self, x, mats, pred, training=True):
        # input_dim = 1
        batch_dim = x.size(dim=0)
        output_dim = pred.size(dim=1)

        loss = torch.nn.MSELoss()

        # loop over outputs of NN
        dnn = (
            torch.stack(
                [
                    torch.autograd.grad(
                        outputs=pred[:, i],
                        inputs=x,
                        grad_outputs=torch.ones(
                            batch_dim, device=self.device, dtype=self.dtype_real
                        ),
                        retain_graph=training or i != output_dim - 1,
                        create_graph=training,
                    )[0]
                    for i in range(output_dim)
                ]
            )
            .transpose(0, 1)
            .squeeze(2)
        )
        # print("dnn", dnn.size())

        # stack up as interleaved Re/Im
        pred = pred.view(2 * batch_dim, output_dim // 2, 1)
        # print("pred", pred.size())

        # this broadcasts over batch+(re/im) dimension
        target = torch.matmul(mats, pred).view(batch_dim, output_dim)
        # print("target", target.size())

        bulk = loss(dnn, target)

        bv = self.model(self.bcx)
        # print("bv", bv.size())
        # print("bcy", self.bcy.size())
        boundary = loss(bv, self.bcy)

        return bulk + boundary


def as_complex(x, n):
    return torch.view_as_complex(x.reshape(-1, 2, n).transpose(1, 2).contiguous())


def mats_num(df, x, device, dtype):
    return torch.stack(
        [torch.tensor(df(xx), device=device, dtype=dtype) for xx in x]
    ).repeat_interleave(2, 0)


def rel_diff(a, b):
    return torch.mean(torch.abs(2 * (a - b) / (a + b)))


if __name__ == "__main__":
    print("Initialising")
    start = time.time()

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    model = NeuralNetwork(layers=h.layers, dtype_real=h.dtype_real).to(device)

    loss_fn = MyLoss(
        x=s.bcx,
        y=s.bcy,
        model=model,
        device=device,
        dtype_real=h.dtype_real,
        dtype_complex=h.dtype_complex,
    )

    optimiser = torch.optim.Adam(
        model.parameters(),
        lr=h.learning_rate,
        betas=(0.9, 0.999),
        eps=1e-8,
        weight_decay=h.l2_regularisation_weight,
        fused=device == "cuda",
    )
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=h.decay)

    x = torch.unsqueeze(
        torch.tensor(
            v.xx,
            device=device,
            requires_grad=True,
            dtype=h.dtype_real,
        ),
        dim=1,
    )  # grad required for validation_metrics loss

    mats = mats_num(s.ode_matrix, x, device=device, dtype=h.dtype_real)

    f = torch.tensor(v.ff, device=device, dtype=h.dtype_complex)

    n = len(s.bcy)

    regions = p.get_regions(h.batch_size)
    dists = [torch.distributions.uniform.Uniform(r["start"], r["end"]) for r in regions]
    x_train = (
        torch.concatenate(
            [dist.sample((r["number"], 1)) for dist, r in zip(dists, regions)]
        )
        .to(device)
        .to(h.dtype_real)
        .requires_grad_()
    )

    mats_train = mats_num(s.ode_matrix, x_train, device=device, dtype=h.dtype_real)

    dur = time.time() - start
    print(f"Initialisation took {dur:.1f}s")

    print("Training")
    start = time.time()

    lrs = []
    validation_metrics = []
    validation_losses = []
    for t in range(h.epochs):
        lrs += scheduler.get_last_lr()
        print(
            f"Epoch {t+1}/{h.epochs} (lr: {lrs[-1]:.4f})\n-------------------------------"
        )

        model.train()
        for batch in range(h.number_batches):
            # Compute prediction error
            pred_train = model(x_train)
            loss_train = loss_fn(x_train, mats_train, pred_train)

            # Backpropagation
            optimiser.zero_grad()
            loss_train.backward()
            optimiser.step()

            if not batch % (h.number_batches // 10):
                print(
                    f"loss: {loss_train:9.6f}  [{h.batch_size*batch:>7d}/{h.batch_size*h.number_batches:>8d}]"
                )

        model.eval()
        pred_valid = model(x)
        loss_valid = loss_fn(x, mats, pred_valid, training=False).detach()
        metric_valid = rel_diff(as_complex(pred_valid.detach(), n), f)
        print(f"validation metric: {metric_valid:.6f}")
        print(f"             loss: {loss_valid:.6f}")

        validation_metrics.append(metric_valid.cpu())
        validation_losses.append(loss_valid.cpu())
        scheduler.step()

    lrs = numpy.array(lrs)
    validation_metrics = numpy.array(validation_metrics)
    validation_losses = numpy.array(validation_losses)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    # validation statistics plot
    val_plot_name = "validation-log.pdf"
    print(f"Plotting {val_plot_name}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.set_xlabel("Epoch")
    ax.set_ylabel("Value")
    ax.set_yscale("log")
    ax.plot(lrs, label="Learning rate")
    ax.plot(validation_metrics, label="Validation metric")
    ax.plot(validation_losses, label="Validation loss")
    ax.legend()
    fig.savefig(val_plot_name, bbox_inches="tight")

    nn = as_complex(pred_valid.detach(), n).cpu().numpy()
    g.plot_validation_inference(v.xx, v.ff, nn)

    model_name = "model.pth"
    print(f"Saving model {model_name}")
    torch.save(model.state_dict(), model_name)
