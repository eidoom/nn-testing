#!/usr/bin/env python3

import math
import torch, matplotlib.pyplot, matplotlib.ticker, numpy

from train import NeuralNetwork, as_complex
import hyperparameters as h, validation as v

if __name__ == "__main__":
    print("Initialising")
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    x = torch.unsqueeze(
        torch.tensor(
            v.xx,
            device=device,
            dtype=h.dtype_real,
        ),
        dim=1,
    )

    model = NeuralNetwork(layers=h.layers, dtype_real=h.dtype_real).to(device)
    model.load_state_dict(torch.load("model.pth"))

    model.eval()
    with torch.no_grad():
        nn = model(x)
    nn = as_complex(nn, 3).cpu().numpy()

    rel_err = numpy.mean(numpy.abs((v.ff - nn) / v.ff), axis=0)
    print(f"mean |relative errors| = {rel_err}")

    plotname = "deviation.pdf"
    print(f"Plotting {plotname}")

    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    weights = numpy.full(v.test_size, 1 / v.test_size)
    ax.xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
    )
    r = numpy.log10(numpy.abs(nn / v.ff)).T
    b = numpy.histogram_bin_edges(r, bins="auto")
    for i, ri in enumerate(r):
        ax.hist(
            ri,
            bins=b,
            histtype="step",
            weights=weights,
            label=f"$f_{i}$",
            # log=True,
        )
    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$\log_{10}(NN/f)$")
    ax.axvline(math.log10(1.001), color="black", linewidth=1)
    ax.axvline(math.log10(0.999), color="black", linewidth=1)
    ax.text(
        0.5,
        0.9,
        "$0.1\%$",
        horizontalalignment="center",
        verticalalignment="center",
        transform=ax.transAxes,
    )
    ax.legend()
    fig.savefig(plotname, bbox_inches="tight")
