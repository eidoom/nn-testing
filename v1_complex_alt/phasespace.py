import system as s


def get_regions(size):
    boundaries = []
    poles = sorted(s.poles)

    if all(abs(s.domain_l - pole) - s.cut > 0 for pole in poles):
        boundaries.append(s.domain_l)

    for pole in poles:
        pl = pole - s.cut
        if s.domain_l < pl < s.domain_r:
            boundaries.append(pl)

        pr = pole + s.cut
        if s.domain_l < pr < s.domain_r:
            boundaries.append(pr)

    if all(abs(s.domain_r - pole) - s.cut > 0 for pole in poles):
        boundaries.append(s.domain_r)

    regions = [
        {"start": a, "end": b} for a, b in zip(boundaries[:-1:2], boundaries[1::2])
    ]

    total = sum(r["end"] - r["start"] for r in regions)

    for i, region in enumerate(regions):
        region["number"] = int((region["end"] - region["start"]) / total * size)

        if i == len(regions) - 1:
            diff = size - sum(r["number"] for r in regions)
            if diff == 1:
                region["number"] += 1
            elif diff != 0:
                raise Exception(f"Unexpected: diff={diff}")

    return regions
