#!/usr/bin/env julia

# https://docs.sciml.ai/NeuralPDE/stable/tutorials/ode/

import NeuralPDE # slow
import Flux, OptimizationOptimisers

linear(u, p, t) = cos(2pi * t)
tspan = (0.0f0, 1.0f0)
u0 = 0.0f0
prob = NeuralPDE.ODEProblem(linear, u0, tspan)

chain = Flux.Chain(Flux.Dense(1, 5, Flux.sigmoid), Flux.Dense(5, 1))

opt = OptimizationOptimisers.Adam(0.1)
alg = NeuralPDE.NNODE(chain, opt)

sol = NeuralPDE.solve(prob, alg, verbose=true, abstol=1f-6, maxiters=200)
println(sol)
