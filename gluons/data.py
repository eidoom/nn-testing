#!/usr/bin/env python3

import numpy, pathlib

if __name__ == "__main__":
    cache = pathlib.Path("data.npy")
    if cache.is_file():
        data = numpy.load(cache)
    else:
        data = numpy.genfromtxt("data/data.csv")
        numpy.save("data.npy", data)

    inputs = data[:, :6]
    output = data[:, 6]

    print(inputs[0], output[0])
