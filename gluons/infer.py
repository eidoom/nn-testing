#!/usr/bin/env python3

import torch, numpy

from model import make_dataset, NeuralNetwork

if __name__ == "__main__":
    model = NeuralNetwork()
    model.load_state_dict(torch.load("model.pth"))
    model.eval()

    data = make_dataset(numpy.load("data.npy"))

    with torch.no_grad():
        for X, y in zip(data[:][0], data[:][1]):
            pred = model(X)
            predicted, actual = pred[0], y[0]
            print(f'Predicted: "{predicted}", Actual: "{actual}"')
