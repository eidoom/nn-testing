#!/usr/bin/env python3

import torch, numpy


class NeuralNetwork(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.linear_tanh_stack = torch.nn.Sequential(
            torch.nn.Linear(6, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 20),
            torch.nn.Tanh(),
            torch.nn.Linear(20, 1),
        )

    def forward(self, x):
        return self.linear_tanh_stack(x)


def train(dataloader, model, loss_fn, optimiser, device):
    size = len(dataloader.dataset)
    model.train()
    for batch, (X, y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)

        # Compute prediction error
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        optimiser.zero_grad()
        loss.backward()
        optimiser.step()

        if batch % 100 == 0:
            loss, current = loss.item(), batch * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")


def test(dataloader, model, loss_fn, device):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float32).sum().item()
    test_loss /= num_batches
    correct /= size
    print(
        f"Test Error: \n Accuracy: {(100*correct):>0.3f}%, Avg loss: {test_loss:>8f} \n"
    )


def make_dataset(data):
    x = torch.tensor(data[:, :6], dtype=torch.float32)

    # standardise target distribution
    # TODO should test destandardised result
    yy = data[:, 6:]
    yy = (yy - yy.mean()) / yy.std()

    # damp outliers
    yy = numpy.tanh(yy)

    y = torch.tensor(yy, dtype=torch.float32)
    return torch.utils.data.TensorDataset(x, y)


if __name__ == "__main__":
    # hyperparameters
    split = 0.8
    learning_rate = 1e-3
    batch_size = 100
    epochs = 5

    data = numpy.load("data.npy")
    train_data = make_dataset(data[: int(len(data) * split)])
    test_data = make_dataset(data[int(len(data) * split) :])

    train_dataloader = torch.utils.data.DataLoader(train_data, batch_size=batch_size)
    test_dataloader = torch.utils.data.DataLoader(test_data, batch_size=batch_size)

    device = "cuda"
    model = NeuralNetwork().to(device)
    loss_fn = torch.nn.MSELoss()
    optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)

    for t in range(epochs):
        print(f"Epoch {t+1}\n-------------------------------")
        train(train_dataloader, model, loss_fn, optimiser, device)
        test(test_dataloader, model, loss_fn, device)

    torch.save(model.state_dict(), "model.pth")

    print("Done!")
