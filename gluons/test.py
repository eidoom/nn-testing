#!/usr/bin/env python3

import torch

if __name__ == "__main__":
    print("cuda:", torch.cuda.is_available())

    x = torch.rand(5, 3)
    print(x)
