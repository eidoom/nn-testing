#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>

// #include <qd/dd_real.h>

// modified to remove access specifiers
#include "analytic/0q5g-analytic.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"

template <typename T>
void run(const int num)
{
	const int legs { 5 };
	Amp0q5g_a<T> amp;
	amp.setMuR2(std::pow(91.188, 2));
	amp.setNf(5);
	amp.setNc(3);

	// fake PDF
	// std::random_device rg;
	// deterministic: generate same random numbers each run by setting same seed 1
	std::mt19937_64 rng(1);
	std::uniform_real_distribution<T> dist(1, 1e3); // 1 GeV -- 1 TeV

	std::ofstream o("data.csv");
	o.setf(std::ios_base::scientific);
	o.precision(16);

	for (int rseed { 1 }; rseed <= num; ++rseed) {
		std::cout << std::setw(7) << rseed << '\n';

		const T sqrtS(dist(rng));
		PhaseSpace<T> ps5(legs, rseed, sqrtS);
		std::vector<MOM<T>> mom { ps5.getPSpoint() };

		amp.setMomenta(mom);

		for (int i { 0 }; i < legs; ++i) {
			// momentum invariants sijs
			// o << 2 * dot(mom[i], mom[(i + 1) % legs]) << ' ';
			o << amp.njetan->lS(i, (i + 1) % legs) << ' ';
		}
		// tr5
		o << amp.njetan->eps5().imag() << ' ';
		// return K-factor wrt finite part of loop
		o << amp.virt().get0().real() / amp.born() << '\n';
	}
}

int main(int argc, char* argv[])
{
	std::cout << std::scientific << std::setprecision(16);
	assert(argc == 2);
	run<double>(std::atoi(argv[1]));
	// run<dd_real>(std::atoi(argv[1]));
}
