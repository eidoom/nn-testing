import torch

import system as s

learning_rate = 1e-2
decay = 0.9
l2_regularisation_weight = 1e-6

batch_size = 1000
number_batches = 200
epochs = 10

dtype_real = torch.float32
dtype_complex = torch.complex64

# R^len(s.bcx) -> C^(#e*#f)
layers = [s.num_in] + [40] * 2 + [2 * s.num_eps * s.num_func]
