#!/usr/bin/env python3

import torch, numpy

from train import NeuralNetwork, as_complex
import hyperparameters as h, validation as v, graphs as g, system as s

if __name__ == "__main__":
    print("Initialising")
    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    x = torch.tensor(
        v.xx,
        device=device,
        dtype=h.dtype_real,
    )

    model = NeuralNetwork(layers=h.layers, dtype_real=h.dtype_real).to(device)
    model.load_state_dict(torch.load("model.pth"))

    model.eval()
    with torch.no_grad():
        nn = model(x)
    nn = as_complex(nn, s.num_eps, s.num_func).cpu().numpy()

    err = numpy.mean(numpy.abs(v.ff - nn), axis=0)
    print(f"MAE = {err}")

    g.plot_validation_inference(v.xx, v.ff, nn)

    g.plot_validation_deviation(v.ff, nn)
