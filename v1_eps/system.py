import math, random
import numpy, scipy.special


def li1(z):
    # singularity at z = 1
    # analytic for z < 1
    # branch cut for z > 1
    return -numpy.log(1.0 - z)


def li2(z):
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.spence.html#scipy.special.spence
    return scipy.special.spence(1.0 - z)


def f(v):
    x = v[0]
    # each row is a coefficient of eps: [eps^0, eps^1, eps^2]
    return [
        [1, 0, 0, 0],
        [0, li1(x), 0, 0],
        [0, 0, li1(x) ** 2, li2(x)],
    ]


num_in = 1
num_eps = 3
num_func = 4

cut = 0.1
poles = [0.0, 1.0]  # poles in f and df

# analytically continued region, solution is complex
domain_l = 1.0
domain_r = domain_l + math.pi

bcx = [[domain_l + 2 * cut], [domain_r - 2 * cut]]
bcy = [f([xx + 0.0j for xx in x]) for x in bcx]


def ode_matrix(v):
    # diverges for x in {0,1}
    # assume form A^0 + eps A^1
    # real valued
    x = v[0]
    return [
        [
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
            [0, 0, 0, 0],
        ],
        [
            [0, 0, 0, 0],
            [1 / (1 - x), 0, 0, 0],
            [0, 2 / (1 - x), 0, 0],
            [0, 1 / x, 0, 0],
        ],
    ]


assert len(bcx) == len(bcy)
assert len(bcx[0]) == num_in
assert len(bcy[0]) == num_eps
assert len(bcy[0][0]) == num_func
p = random.random()
m = ode_matrix([p])
assert len(m) == 2
assert len(m[0]) == num_func
assert len(m[0][0]) == num_func
