import numpy
import system as s
from phasespace import get_regions

test_size = 1000

xx = numpy.expand_dims(
    numpy.concatenate(
        [
            numpy.linspace(
                start=region["start"],
                stop=region["end"],
                num=region["number"],
                dtype=numpy.cdouble,
            )
            for region in get_regions(test_size)
        ]
    ),
    axis=1,
)

ff = numpy.array([s.f(x) for x in xx], dtype=numpy.cdouble)

xx = xx.real
