(* ::Package:: *)

f[x,e]=f[#][x,e]&/@Range[4]
m[x,e]=e{{0,0,0,0},{1/(1-x),0,0,0},{0,2/(1-x),0,0},{0,1/x,0,0}}
de = D[f[x,e],x]==m[x,e] . f[x,e]


s[x,e]={1,e PolyLog[1,x],e^2 PolyLog[1,x]^2,e^2 PolyLog[2,x]}
bc = f[x,e]==s[x,e]/.x->2


m[x,e]//MatrixForm


{sol} = DSolve[de&&bc,f[x,e],x]//FullSimplify


f[x,e]/.sol//MatrixForm


Plot[{Re@Values@sol,Im@Values@sol}/.e->1,{x,1.1,1.1+Pi}]
