import math
import numpy, scipy.special


def li1(z):
    # singularity at z = 1
    # analytic for z < 1
    # branch cut for z > 1
    return -numpy.log(1.0 - z)


def li2(z):
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.special.spence.html#scipy.special.spence
    # alt: https://www.mpmath.org/doc/current/functions/zeta.html#polylog
    return scipy.special.spence(1.0 - z)


cut = 0.1
poles = [0.0, 1.0]  # poles in f and df

# analytically continued region, solution is complex
domain_l = 1.0
domain_r = domain_l + math.pi

x = 1.2 + 0.0j
bcx = [x]
bcy = [1.0, li1(x), li2(x)]
# bcy = [1.0, li1(x), li2(x), li1(x) ** 2, li1(x) * li2(x)]


def ode_matrix(v):
    # diverges for x in {0,1}
    x = v[0]
    return [
        [0.0, 0.0, 0.0],
        [1.0 / (1.0 - x), 0.0, 0.0],
        [0.0, 1.0 / x, 0.0],
        # [0.0, 0.0, 0.0, 0.0, 0.0],
        # [1.0 / (1.0 - x), 0.0, 0.0, 0.0, 0.0],
        # [0.0, 1.0 / x, 0.0, 0.0, 0.0],
        # [0.0, 2.0 / (1.0 - x), 0.0, 0.0, 0.0],
        # [0.0, 0.0, 1.0 / (1.0 - x), 1.0 / x, 0.0],
    ]
