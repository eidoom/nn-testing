#!/usr/bin/env python3

import time
import torch, numpy, matplotlib.pyplot, matplotlib.ticker


class NeuralNetwork(torch.nn.Module):
    def __init__(self, layers):
        super().__init__()

        tmp = []
        for a, b in zip(layers[:-2], layers[1:-1]):
            tmp += [torch.nn.Linear(a, b), torch.nn.Tanh()]

        self.stack = torch.nn.Sequential(*tmp, torch.nn.Linear(layers[-2], layers[-1]))

    def forward(self, x):
        return self.stack(x)


class MyLoss(torch.nn.Module):
    def __init__(self, df, x, y, model, device):
        super().__init__()
        self.df = df
        self.x = torch.tensor([x], device=device, dtype=torch.float32)
        self.y = torch.tensor([y], device=device, dtype=torch.float32)
        self.model = model
        self.device = device

    def forward(self, x, pred, training=True):
        loss = torch.nn.MSELoss()

        (dnn,) = torch.autograd.grad(
            outputs=pred,
            inputs=x,
            grad_outputs=torch.ones_like(x, device=self.device),
            create_graph=training,
            retain_graph=training,
        )

        target = self.df(x)
        bulk = loss(dnn, target)

        bv = self.model(self.x)
        boundary = loss(bv, self.y)

        return bulk + boundary


def ff(x):
    return x**3 + x**2 + x + 5.0


def init_torch():
    torch.set_default_dtype(torch.float32)

    device = "cuda" if torch.cuda.is_available() else "cpu"
    print(f"Device: {device}")

    return device


def validation_data(f_func, domain_l, domain_r, test_size, device, requires_grad=False):
    print("Preparing validation data")
    x = torch.reshape(
        torch.linspace(
            domain_l, domain_r, test_size, device=device, requires_grad=requires_grad
        ),
        (test_size, 1),
    )  # grad required for validation_metrics loss
    f = f_func(x.detach())  # no grad
    return x, f


def training(
    loss_fn,
    model,
    device,
    learning_rate,
    decay,
    domain_l,
    domain_r,
    batch_size,
    number_batches,
    epochs,
    x_valid,
    f_valid,
):
    print("Training")
    print(f"Batch size: {batch_size}")
    start = time.time()

    optimiser = torch.optim.Adam(model.parameters(), lr=learning_rate)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimiser, gamma=decay)

    dist = torch.distributions.uniform.Uniform(domain_l, domain_r)

    lrs = []
    validation_metrics = []
    validation_losses = []
    for t in range(1, 1 + epochs):
        lrs.append(scheduler.get_last_lr())
        print(
            f"Epoch {t}/{epochs} (lr: {lrs[-1][0]:.4f})\n-------------------------------"
        )

        model.train()
        for batch in range(number_batches):
            # Compute prediction error
            x_train = dist.sample((batch_size, 1)).to(device).requires_grad_()
            pred = model(x_train)
            loss = loss_fn(x_train, pred)

            # Backpropagation
            optimiser.zero_grad()
            loss.backward()
            optimiser.step()

            if not batch % (number_batches // 10):
                print(f"loss: {loss:9.6f}  [{batch:>3d}/{number_batches:>4d}]")

        model.eval()
        pred = model(x_valid)
        validation_loss = loss_fn(x_valid, pred, training=False).detach()
        validation_metric = torch.mean(torch.abs((pred.detach() - f_valid) / f_valid))
        print(f"validation metric: {validation_metric:.6f}")
        print(f"             loss: {validation_loss:.6f}")

        validation_metrics.append(validation_metric.cpu())
        validation_losses.append(validation_loss.cpu())
        scheduler.step()

    lrs = numpy.array(lrs)
    validation_metrics = numpy.array(validation_metrics)
    validation_losses = numpy.array(validation_losses)

    dur = time.time() - start
    print(f"Training took {dur:.1f}s")

    return lrs, validation_metrics, validation_losses


def inference(model, x):
    print("Running NN")
    model.eval()
    with torch.no_grad():
        return model(x)


def plot_validation(
    lrs, validation_metrics, validation_losses, name="validation-log.pdf"
):
    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.plot(lrs, label="Learning rate")
    ax.plot(validation_metrics, label="Validation metric")
    ax.plot(validation_losses, label="Validation loss")
    ax.legend()
    ax.set_yscale("log")
    fig.savefig(name)


def plot_inference(x, f, nn, name="inference.pdf"):
    fig, ax = matplotlib.pyplot.subplots(tight_layout=True)
    ax.scatter(x, f, label="Truth")
    ax.scatter(x, nn, label="Surrogate")
    ax.legend()
    fig.savefig(name)


def plot_deviation(f, nn, name="deviation.pdf"):
    rat = numpy.log10(nn / f)

    w = 6.4
    r = 2 / (1 + 5**0.5)
    fig, axs = matplotlib.pyplot.subplots(
        nrows=2,
        sharex="col",
        tight_layout=True,
        figsize=(w, w / r),
        gridspec_kw={
            "hspace": 0,
        },
    )
    for ax in axs:
        ax.xaxis.set_major_locator(
            matplotlib.ticker.MaxNLocator(nbins="auto", symmetric=True)
        )
        ax.hist(
            rat,
            bins=50,
            histtype="step",
            weights=numpy.full(test_size, 1 / test_size),
        )
        ax.set_ylabel("Proportion")
    axs[1].set_xlabel(r"$\log_{10}(NN/f)$")
    axs[1].set_yscale("log")
    fig.savefig(name)


if __name__ == "__main__":
    print("Initialising")

    # hyperparameters
    learning_rate = 1e-2
    decay = 0.9
    batch_size = 100
    number_batches = 100
    epochs = 100
    test_size = 1000

    domain_l = -1
    domain_r = -domain_l

    device = init_torch()

    model = NeuralNetwork(layers=[1] + [128] * 3 + [1]).to(device)

    loss_fn = MyLoss(
        df=lambda x: 3.0 * x**2 + 2.0 * x + 1.0,
        x=0.0,
        y=ff(0.0),
        model=model,
        device=device,
    )

    x, f = validation_data(
        ff, domain_l, domain_r, test_size, device, requires_grad=True
    )

    lrs, validation_metrics, validation_losses = training(
        loss_fn,
        model,
        device,
        learning_rate,
        decay,
        domain_l,
        domain_r,
        batch_size,
        number_batches,
        epochs,
        x,
        f,
    )

    x = x.detach()
    nn = inference(model, x)

    x = x.cpu().numpy()
    f = f.cpu().numpy()
    nn = nn.cpu().numpy()

    print("Plotting")

    plot_validation(lrs, validation_metrics, validation_losses)

    plot_inference(x, f, nn)

    plot_deviation(f, nn)

    print("Saving model")
    torch.save(model.state_dict(), "model.pth")

    print("Done!")
