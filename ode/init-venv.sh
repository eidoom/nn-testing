if [ "$0" != "-bash" ]; then
	echo "This must be run as \`source init-venv.sh\`"
fi

SUF=torch
DIR=../.venv-${SUF}
REQ=../requirements-${SUF}.txt

if [ ! -d "${DIR}" ]; then
	python3 -m venv "${DIR}"
	source "${DIR}/bin/activate"
	if [ -f "${REQ}" ]; then
		python3 -m pip install -r "${REQ}"
	fi
else
	source "${DIR}/bin/activate"
fi

echo "venv activated"
echo "use \`deactivate\` when done"
