#!/usr/bin/env python3

import math
import torch, numpy, matplotlib.pyplot, matplotlib.ticker

from ode import NeuralNetwork, ff, inference, init_torch, validation_data


def plot_deviation_alt(f, nn):
    rat = nn / f

    w = 6.4
    r = (1 + 5**0.5) / 2
    fig, ax = matplotlib.pyplot.subplots(
        nrows=1,
        tight_layout=True,
        figsize=(w, w / r),
    )

    mini = numpy.min(rat)
    maxi = numpy.max(rat)

    bins = numpy.logspace(
        math.log10(mini),
        math.log10(maxi),
        num=20,
        base=10,
    )

    ax.hist(
        rat,
        bins=bins,
        histtype="step",
        weights=numpy.full(len(f), 1 / len(f)),
    )

    lim = 1.1 * max(1 - mini, maxi - 1)
    ax.set_xlim(1 - lim, 1 + lim)

    ax.set_xscale("log")

    ax.xaxis.set_minor_locator(matplotlib.ticker.MaxNLocator(6))

    ax.set_ylabel("Proportion")
    ax.set_xlabel(r"$NN/f$")

    fig.savefig("cf.pdf")


if __name__ == "__main__":
    device = init_torch()
    x, f = validation_data(ff, -1, 1, int(1e6), device)

    model = NeuralNetwork(layers=[1] + [128] * 3 + [1]).to(device)
    model.load_state_dict(torch.load("model.pth"))

    nn = inference(model, x)

    f = f.cpu().numpy()
    nn = nn.cpu().numpy()

    plot_deviation_alt(f, nn)
