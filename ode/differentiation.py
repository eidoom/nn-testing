#!/usr/bin/env python3

import torch


def f1(x):
    return x**3 + x**2 + x + 5


def df1(x):
    return 3 * x**2 + 2 * x + 1


if __name__ == "__main__":
    # input

    x = torch.rand(2, requires_grad=True)

    # 1) backward > grad

    y = f1(x)

    y.backward(torch.ones_like(y))

    print(x.grad)

    # 2) autograd.grad

    z = f1(x)

    (dz,) = torch.autograd.grad(
        outputs=z,
        inputs=x,
        grad_outputs=torch.ones_like(z),
    )

    print(dz)

    # validation

    with torch.no_grad():
        dy = df1(x)

    print(dy)
