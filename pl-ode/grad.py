#!/usr/bin/env python3

import time

import torch

import main


if __name__ == "__main__":
    model = main.NeuralNetwork(layers=[2] + [5] * 2 + [3]).to("cuda")

    x = torch.rand((4, 2), requires_grad=True, device="cuda")
    print("input: ", x.size())
    print(x)

    z = model(x)
    print("output: ", z.size())
    print(z)

    # # 0

    # dz = []
    # for zz in z:
    #     zz.backward(retain_graph=True, create_graph=True)
    #     dz.append(x.grad)
    # dz = torch.stack(dz)

    # print(dz)

    # # 1

    # dz = torch.empty((z.size(dim=0), x.size(dim=0)), device="cuda")
    # for i, zz in enumerate(z):
    #     zz.backward(retain_graph=True, create_graph=True)
    #     dz[i] = x.grad

    # print(dz)

    # 3

    t0 = time.time()
    dz = torch.stack(
        [
            torch.autograd.grad(
                outputs=(zz := z[:, i]),
                inputs=x,
                grad_outputs=torch.ones_like(zz),
                retain_graph=True,
                create_graph=True,
            )[0]
            for i in range(z.size(dim=1))
        ]
    ).transpose(0, 1)

    print("d(output)/d(input): ", dz.size())
    print(dz)
    print("time: ", 1000 * (time.time() - t0), " ms")

    print("VRAM: ", torch.cuda.max_memory_allocated() / 1e6, " MB")
