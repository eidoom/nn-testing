#!/usr/bin/env python3

import torch


def f(v: torch.Tensor) -> torch.Tensor:
    """
    [
        [x, 1],
        [x**2, 0],
        [2 * x, 3 * x**2],
    ]
    """
    x = v[:, 0]
    return torch.stack(
        (
            torch.stack((x, torch.ones_like(x)), dim=1),
            torch.stack((x**2, torch.zeros_like(x)), dim=1),
            torch.stack((2 * x, 3 * x**2), dim=1),
        ),
        dim=1,
    )


def df(v):
    """
    [
        [1, 0],
        [2 * x, 0],
        [2, 6 * x],
    ]
    """
    x = v[:, 0]
    return torch.stack(
        (
            torch.stack((torch.ones_like(x), torch.zeros_like(x)), dim=1),
            torch.stack((2 * x, torch.zeros_like(x)), dim=1),
            torch.stack((2 * torch.ones_like(x), 6 * x), dim=1),
        ),
        dim=1,
    )


if __name__ == "__main__":
    # input

    x = torch.rand((4, 1), requires_grad=True)

    # 1) backward > grad

    y = f(x)

    y.backward(torch.ones_like(y))

    dy = x.grad

    print(dy.flatten())

    # 2) autograd.grad

    z = f(x)

    (dz,) = torch.autograd.grad(
        outputs=z,
        inputs=x,
        grad_outputs=torch.ones_like(z),
    )

    print(dz.flatten())

    # validation

    with torch.no_grad():
        dv = torch.sum(df(x), dim=(1, 2))

    print(dv)
