(* ::Package:: *)

f[x]={f0[x],f1[x],f2[x]}
m[x]={{0,0,0},{1/(1-x),0,0},{0,1/x,0}}
de = D[f[x],x]==m[x] . f[x]


{sol} = DSolve[de,f[x],x]/.{C[3]->1,C[2]->0,C[1]->0}


Plot[Re@Values@sol,{x,1.1,7}]


Plot[Im@Values@sol,{x,1.1,7}]
